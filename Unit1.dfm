object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 
    'BioFilmAnalyzer -- Semi-automatic processing of fluorescent micr' +
    'oscopic images focused on biofilm research issues'
  ClientHeight = 916
  ClientWidth = 1093
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 690
    Top = 0
    Height = 916
    Beveled = True
    Color = clActiveBorder
    MinSize = 130
    ParentColor = False
    OnMoved = Splitter1Moved
    ExplicitLeft = 892
    ExplicitTop = 23
  end
  object Panel1: TPanel
    Left = 693
    Top = 0
    Width = 400
    Height = 916
    Align = alClient
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 0
    object Image3: TImage
      Left = 1
      Top = 466
      Width = 400
      Height = 400
      Stretch = True
      Transparent = True
      Visible = False
    end
    object CGauge1: TCGauge
      Left = 1
      Top = 81
      Width = 398
      Height = 16
      Align = alTop
      ExplicitTop = 50
      ExplicitWidth = 400
    end
    object Image1: TImage
      Left = 1
      Top = 97
      Width = 398
      Height = 369
      Align = alClient
      Stretch = True
      Transparent = True
      OnClick = Image1Click
      ExplicitLeft = 177
      ExplicitTop = 66
      ExplicitWidth = 454
      ExplicitHeight = 400
    end
    object Image2: TImage
      Left = 1
      Top = 466
      Width = 398
      Height = 400
      Align = alBottom
      ParentShowHint = False
      ShowHint = False
      Stretch = True
      Transparent = True
      OnDblClick = Image2DblClick
      OnMouseDown = Image2MouseDown
      ExplicitLeft = 2
      ExplicitTop = 441
      ExplicitWidth = 630
    end
    object Label1: TLabel
      Left = 113
      Top = 448
      Width = 167
      Height = 16
      Align = alCustom
      Alignment = taCenter
      Caption = 'Drag and drop image(s) here'
      Transparent = True
    end
    object Image4: TImage
      Left = 1
      Top = 66
      Width = 400
      Height = 400
      Stretch = True
      Transparent = True
      Visible = False
      OnClick = Image4Click
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 398
      Height = 80
      Align = alTop
      TabOrder = 0
      object RadioGroup2: TRadioGroup
        Left = 1
        Top = 1
        Width = 80
        Height = 78
        Align = alLeft
        Caption = 'Now count ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -10
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          'RED'
          'GREEN'
          'BLUE')
        ParentFont = False
        TabOrder = 0
      end
      object RadioGroup3: TRadioGroup
        Left = 81
        Top = 1
        Width = 96
        Height = 78
        Align = alLeft
        Caption = 'Detect color if ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -10
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 1
        Items.Strings = (
          'R-G,G-R > T'
          'R,G,B > T'
          'R+G+B > T')
        ParentFont = False
        TabOrder = 1
      end
      object GroupBox3: TGroupBox
        Left = 177
        Top = 1
        Width = 220
        Height = 78
        Align = alClient
        Caption = 'Sub-population fraction is given by ...'
        TabOrder = 2
        object Label17: TLabel
          Left = 6
          Top = 31
          Width = 220
          Height = 16
          Caption = 'S=_____________________________'
        end
        object CheckBox2: TCheckBox
          Left = 30
          Top = 24
          Width = 67
          Height = 17
          Caption = 'Red +'
          TabOrder = 0
          OnClick = CheckBox2Click
        end
        object CheckBox3: TCheckBox
          Left = 30
          Top = 53
          Width = 57
          Height = 17
          Caption = 'Red +'
          TabOrder = 1
          OnClick = CheckBox2Click
        end
        object CheckBox4: TCheckBox
          Left = 93
          Top = 24
          Width = 76
          Height = 17
          Caption = 'Green +'
          TabOrder = 2
          OnClick = CheckBox2Click
        end
        object CheckBox5: TCheckBox
          Left = 93
          Top = 53
          Width = 76
          Height = 17
          Caption = 'Green +'
          TabOrder = 3
          OnClick = CheckBox2Click
        end
        object CheckBox6: TCheckBox
          Left = 167
          Top = 24
          Width = 50
          Height = 17
          Caption = 'Blue'
          TabOrder = 4
          OnClick = CheckBox2Click
        end
        object CheckBox7: TCheckBox
          Left = 167
          Top = 53
          Width = 50
          Height = 17
          Caption = 'Blue'
          TabOrder = 5
          OnClick = CheckBox2Click
        end
        object CheckListBox3: TCheckListBox
          Left = -600
          Top = 64
          Width = 121
          Height = 97
          TabOrder = 6
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 866
      Width = 398
      Height = 49
      Align = alBottom
      TabOrder = 1
      object Label2: TLabel
        Left = 1
        Top = 32
        Width = 396
        Height = 16
        Align = alBottom
        Alignment = taCenter
        Caption = 'Adjust sensitivity threshold'
        ExplicitWidth = 152
      end
      object TrackBar1: TTrackBar
        Left = 1
        Top = 1
        Width = 396
        Height = 31
        Align = alClient
        Max = 255
        Frequency = 16
        Position = 30
        TabOrder = 0
        TickMarks = tmTopLeft
        OnChange = TrackBar1Change
      end
      object Edit9: TEdit
        Left = 345
        Top = 27
        Width = 56
        Height = 24
        Color = cl3DLight
        Enabled = False
        TabOrder = 1
        Text = '30'
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 690
    Height = 916
    Align = alLeft
    TabOrder = 1
    object Panel7: TPanel
      Left = 1
      Top = 212
      Width = 688
      Height = 393
      Align = alClient
      TabOrder = 0
      object StringGrid3: TStringGrid
        Left = 171
        Top = 23
        Width = 170
        Height = 369
        Align = alLeft
        Color = clLime
        ColCount = 7
        DefaultColWidth = 19
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 100
        FixedRows = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -10
        Font.Name = 'Agency FB'
        Font.Style = []
        GradientEndColor = clLime
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing]
        ParentFont = False
        TabOrder = 0
        OnClick = StringGrid3Click
        ColWidths = (
          19
          19
          19
          19
          19
          19
          19)
        RowHeights = (
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15)
      end
      object StringGrid4: TStringGrid
        Left = 1
        Top = 23
        Width = 170
        Height = 369
        Align = alLeft
        Color = clRed
        ColCount = 7
        DefaultColWidth = 19
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 100
        FixedRows = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -10
        Font.Name = 'Agency FB'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing]
        ParentFont = False
        TabOrder = 1
        OnClick = StringGrid4Click
        ColWidths = (
          19
          19
          19
          19
          19
          19
          19)
        RowHeights = (
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15)
      end
      object StringGrid1: TStringGrid
        Left = 511
        Top = 23
        Width = 176
        Height = 369
        Align = alClient
        Color = cl3DLight
        ColCount = 7
        DefaultColWidth = 19
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 100
        FixedRows = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -10
        Font.Name = 'Agency FB'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected]
        ParentFont = False
        TabOrder = 2
        ColWidths = (
          19
          19
          19
          19
          19
          19
          19)
        RowHeights = (
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15)
      end
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 686
        Height = 22
        Align = alTop
        TabOrder = 3
        object StringGrid2: TStringGrid
          Left = 1
          Top = 1
          Width = 170
          Height = 20
          Align = alLeft
          ColCount = 7
          DefaultColWidth = 19
          DefaultRowHeight = 15
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -10
          Font.Name = 'Agency FB'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing]
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 0
          OnClick = CheckBox2Click
          OnExit = StringGrid2Click
          ColWidths = (
            19
            19
            19
            19
            19
            19
            19)
          RowHeights = (
            15)
        end
        object StringGrid5: TStringGrid
          Left = 171
          Top = 1
          Width = 170
          Height = 20
          Align = alLeft
          Color = clBtnFace
          ColCount = 7
          DefaultColWidth = 19
          DefaultRowHeight = 15
          Enabled = False
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -10
          Font.Name = 'Agency FB'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing]
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 1
          ColWidths = (
            19
            19
            19
            19
            19
            19
            19)
          RowHeights = (
            15)
        end
        object StringGrid6: TStringGrid
          Left = 341
          Top = 1
          Width = 170
          Height = 20
          Align = alLeft
          Color = clBtnFace
          ColCount = 7
          DefaultColWidth = 19
          DefaultRowHeight = 15
          Enabled = False
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -10
          Font.Name = 'Agency FB'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing]
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 2
          ColWidths = (
            19
            19
            19
            19
            19
            19
            19)
          RowHeights = (
            15)
        end
        object StringGrid8: TStringGrid
          Left = 511
          Top = 1
          Width = 174
          Height = 20
          Align = alClient
          Color = clBtnFace
          ColCount = 7
          DefaultColWidth = 19
          DefaultRowHeight = 15
          Enabled = False
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -10
          Font.Name = 'Agency FB'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing]
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 3
          ColWidths = (
            19
            19
            19
            19
            19
            19
            19)
          RowHeights = (
            15)
        end
      end
      object StringGrid7: TStringGrid
        Left = 341
        Top = 23
        Width = 170
        Height = 369
        Align = alLeft
        Color = clAqua
        ColCount = 7
        DefaultColWidth = 19
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 100
        FixedRows = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -10
        Font.Name = 'Agency FB'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected]
        ParentFont = False
        TabOrder = 4
        OnClick = StringGrid7Click
        ColWidths = (
          19
          19
          19
          19
          19
          19
          19)
        RowHeights = (
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15
          15)
      end
    end
    object ScrollBox1: TScrollBox
      Left = 1
      Top = 605
      Width = 688
      Height = 310
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 684
        Height = 306
        TabOrder = 0
        object Label10: TLabel
          Left = 480
          Top = 58
          Width = 192
          Height = 16
          Caption = 'Effective range of single cell sizes'
        end
        object Label11: TLabel
          Left = 477
          Top = 82
          Width = 20
          Height = 16
          Caption = 'Min'
        end
        object Label12: TLabel
          Left = 477
          Top = 113
          Width = 23
          Height = 16
          Caption = 'Max'
        end
        object Label13: TLabel
          Left = 535
          Top = 0
          Width = 145
          Height = 16
          Caption = 'iterations of cell selection'
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 474
          Height = 304
          Align = alLeft
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 1
            Top = 104
            Width = 472
            Height = 100
            Align = alTop
            Caption = 'Green channel statistics'
            TabOrder = 0
            object Label4: TLabel
              Left = 10
              Top = 20
              Width = 318
              Height = 16
              Caption = 'Total pixels above the threshold in the "GREEN" channel'
            end
            object Label5: TLabel
              Left = 20
              Top = 49
              Width = 327
              Height = 16
              Caption = '                    cells within the given range of average size'
            end
            object Label6: TLabel
              Left = 166
              Top = 79
              Width = 298
              Height = 16
              Caption = 'The image contains effectively                         cells'
            end
            object Edit1: TEdit
              Left = 354
              Top = 10
              Width = 80
              Height = 24
              TabOrder = 0
            end
            object Edit2: TEdit
              Left = 10
              Top = 39
              Width = 63
              Height = 24
              TabOrder = 1
            end
            object Edit3: TEdit
              Left = 354
              Top = 39
              Width = 80
              Height = 24
              TabOrder = 2
            end
            object Edit4: TEdit
              Left = 354
              Top = 69
              Width = 80
              Height = 24
              TabOrder = 3
            end
          end
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 472
            Height = 103
            Align = alTop
            Caption = 'Red channel statistics'
            TabOrder = 1
            object Label7: TLabel
              Left = 30
              Top = 20
              Width = 303
              Height = 16
              Caption = 'Total pixels above the threshold in the "RED" channel'
            end
            object Label8: TLabel
              Left = 20
              Top = 49
              Width = 327
              Height = 16
              Caption = '                    cells within the given range of average size'
            end
            object Label3: TLabel
              Left = 166
              Top = 79
              Width = 298
              Height = 16
              Caption = 'The image contains effectively                         cells'
            end
            object Edit5: TEdit
              Left = 354
              Top = 10
              Width = 80
              Height = 24
              TabOrder = 0
            end
            object Edit6: TEdit
              Left = 10
              Top = 39
              Width = 63
              Height = 24
              TabOrder = 1
            end
            object Edit7: TEdit
              Left = 354
              Top = 39
              Width = 80
              Height = 24
              TabOrder = 2
            end
            object Edit8: TEdit
              Left = 354
              Top = 69
              Width = 80
              Height = 24
              TabOrder = 3
            end
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 204
            Width = 472
            Height = 103
            Align = alTop
            Caption = 'Blue channel statistics'
            TabOrder = 2
            object Label18: TLabel
              Left = 30
              Top = 20
              Width = 308
              Height = 16
              Caption = 'Total pixels above the threshold in the "BLUE" channel'
            end
            object Label19: TLabel
              Left = 20
              Top = 49
              Width = 327
              Height = 16
              Caption = '                    cells within the given range of average size'
            end
            object Label20: TLabel
              Left = 166
              Top = 79
              Width = 298
              Height = 16
              Caption = 'The image contains effectively                         cells'
            end
            object Edit12: TEdit
              Left = 354
              Top = 10
              Width = 80
              Height = 24
              TabOrder = 0
            end
            object Edit13: TEdit
              Left = 10
              Top = 39
              Width = 63
              Height = 24
              TabOrder = 1
            end
            object Edit14: TEdit
              Left = 354
              Top = 39
              Width = 80
              Height = 24
              TabOrder = 2
            end
            object Edit15: TEdit
              Left = 354
              Top = 69
              Width = 80
              Height = 24
              TabOrder = 3
            end
          end
        end
        object TrackBar3: TTrackBar
          Left = 546
          Top = 105
          Width = 144
          Height = 40
          Max = 255
          Frequency = 16
          Position = 255
          TabOrder = 1
          OnChange = TrackBar3Change
        end
        object TrackBar2: TTrackBar
          Left = 548
          Top = 75
          Width = 135
          Height = 34
          Max = 255
          Frequency = 16
          TabOrder = 2
          OnChange = TrackBar2Change
        end
        object ComboBox1: TComboBox
          Left = 482
          Top = 0
          Width = 51
          Height = 24
          ItemIndex = 2
          TabOrder = 3
          Text = '5x'
          Items.Strings = (
            '1x'
            '3x'
            '5x'
            '7x'
            '9x')
        end
        object Button3: TButton
          Left = 481
          Top = 135
          Width = 199
          Height = 28
          Caption = 'Re-apply cell size range'
          TabOrder = 4
          OnClick = Button3Click
        end
        object Button1: TButton
          Left = 482
          Top = 22
          Width = 201
          Height = 35
          Caption = 'Re-analyze current image'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnClick = Button1Click
        end
        object CheckBox1: TCheckBox
          Left = 480
          Top = 163
          Width = 199
          Height = 21
          Caption = 'Fix current effective cell sizes'
          TabOrder = 6
          OnClick = CheckBox1Click
        end
        object Edit10: TEdit
          Left = 502
          Top = 77
          Width = 46
          Height = 24
          Color = cl3DLight
          Enabled = False
          TabOrder = 7
        end
        object Edit11: TEdit
          Left = 502
          Top = 108
          Width = 46
          Height = 24
          Color = cl3DLight
          Enabled = False
          TabOrder = 8
        end
        object Button2: TButton
          Left = 476
          Top = 279
          Width = 208
          Height = 27
          Caption = '> > > Export results to Excel > > >'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          OnClick = Button2Click
        end
        object GroupBox5: TGroupBox
          Left = 477
          Top = 181
          Width = 204
          Height = 100
          Caption = 'Multi-threshold analysis'
          TabOrder = 10
          object Label21: TLabel
            Left = 4
            Top = 49
            Width = 49
            Height = 16
            Caption = 'Sstep = '
          end
          object Label22: TLabel
            Left = 4
            Top = 75
            Width = 49
            Height = 16
            Caption = 'Smax = '
          end
          object Button4: TButton
            Left = 3
            Top = 17
            Width = 102
            Height = 25
            Caption = 'Run analysis'
            TabOrder = 0
            OnClick = Button4Click
          end
          object CheckBox8: TCheckBox
            Left = 111
            Top = 21
            Width = 90
            Height = 17
            Caption = 'Normalized'
            TabOrder = 1
          end
          object Edit16: TEdit
            Left = 49
            Top = 45
            Width = 56
            Height = 24
            Color = cl3DLight
            Enabled = False
            TabOrder = 2
            Text = '300'
          end
          object Edit17: TEdit
            Left = 49
            Top = 71
            Width = 48
            Height = 24
            Color = cl3DLight
            Enabled = False
            TabOrder = 3
            Text = '3000'
          end
          object TrackBar4: TTrackBar
            Left = 96
            Top = 45
            Width = 105
            Height = 24
            TabOrder = 4
            OnChange = TrackBar4Change
          end
          object TrackBar5: TTrackBar
            Left = 96
            Top = 70
            Width = 105
            Height = 24
            TabOrder = 5
            OnChange = TrackBar5Change
          end
        end
      end
    end
    object TabControl1: TTabControl
      Left = 1
      Top = 1
      Width = 688
      Height = 211
      Align = alTop
      TabOrder = 2
      Tabs.Strings = (
        'Multi-layer biofilm structure'
        'Cells sub-population statistics'
        'Npix(T) histogram'
        'Nobj(T) histogram')
      TabIndex = 0
      OnChange = TabControl1Change
      object UpDown1: TUpDown
        Left = 664
        Top = -20
        Width = 24
        Height = 40
        Min = -1
        Max = 0
        TabOrder = 2
        OnClick = UpDown1Click
      end
      object Chart1: TChart
        Left = 4
        Top = 384
        Width = 680
        Height = 180
        BackWall.Brush.Style = bsClear
        BackWall.Brush.Gradient.Direction = gdBottomTop
        BackWall.Brush.Gradient.EndColor = clWhite
        BackWall.Brush.Gradient.StartColor = 15395562
        BackWall.Brush.Gradient.Visible = True
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = clSilver
        Gradient.Visible = True
        LeftWall.Color = 14745599
        Legend.ColorWidth = 30
        Legend.Font.Name = 'Verdana'
        Legend.Shadow.Transparency = 0
        Legend.Symbol.Width = 30
        MarginRight = 6
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.LabelsFormat.Font.Name = 'Verdana'
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        Chart3DPercent = 1
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Axis.Color = 4210752
        LeftAxis.Grid.Color = 11119017
        LeftAxis.LabelsFormat.Font.Name = 'Verdana'
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Font.Name = 'Verdana'
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.LabelsFormat.Font.Name = 'Verdana'
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.LabelsFormat.Font.Name = 'Verdana'
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        View3DWalls = False
        Zoom.Animated = True
        Align = alClient
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Label9: TLabel
          Left = 232
          Top = 191
          Width = 34
          Height = 16
          Caption = 'Green'
        end
        object Label14: TLabel
          Left = 54
          Top = 191
          Width = 22
          Height = 16
          Caption = 'Red'
        end
        object Label15: TLabel
          Left = 551
          Top = 191
          Width = 92
          Height = 16
          Caption = 'Sub-populations'
        end
        object Label16: TLabel
          Left = 405
          Top = 191
          Width = 24
          Height = 16
          Caption = 'Blue'
        end
        object Series2: TPointSeries
          HoverElement = [heCurrent]
          Marks.Style = smsLabelValue
          SeriesColor = clRed
          Title = 'Red'
          ClickableLine = False
          Pointer.Brush.Color = clRed
          Pointer.HorizSize = 8
          Pointer.InflateMargins = True
          Pointer.Pen.Width = 3
          Pointer.Pen.Visible = False
          Pointer.Style = psCircle
          Pointer.VertSize = 8
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series1: TPointSeries
          HoverElement = [heCurrent]
          Marks.Style = smsLabelValue
          SeriesColor = clGreen
          Title = 'Green'
          ClickableLine = False
          Pointer.HorizSize = 8
          Pointer.InflateMargins = True
          Pointer.Style = psCircle
          Pointer.VertSize = 8
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series7: TPointSeries
          HoverElement = [heCurrent]
          Title = 'Blue'
          ClickableLine = False
          Pointer.HorizSize = 8
          Pointer.InflateMargins = True
          Pointer.Style = psCircle
          Pointer.VertSize = 8
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series5: TPointSeries
          HoverElement = [heCurrent]
          Marks.Style = smsLabelValue
          SeriesColor = clGreen
          Title = 'Fraction'
          ClickableLine = False
          Pointer.Brush.Color = clGray
          Pointer.HorizSize = 8
          Pointer.InflateMargins = True
          Pointer.Pen.Width = 2
          Pointer.Style = psDiamond
          Pointer.VertSize = 8
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series4: TLineSeries
          HoverElement = [heCurrent]
          Legend.Visible = False
          DataSource = Series2
          SeriesColor = clRed
          ShowInLegend = False
          Brush.BackColor = clDefault
          LinePen.Width = 5
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          object TeeFunction2: TAverageTeeFunction
          end
        end
        object Series3: TLineSeries
          HoverElement = [heCurrent]
          Legend.Visible = False
          DataSource = Series1
          SeriesColor = clGreen
          ShowInLegend = False
          Brush.BackColor = clDefault
          LinePen.Width = 5
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          object TeeFunction1: TAverageTeeFunction
          end
        end
        object Series6: TLineSeries
          HoverElement = [heCurrent]
          Legend.Visible = False
          DataSource = Series1
          SeriesColor = 8404992
          ShowInLegend = False
          Brush.BackColor = clDefault
          LinePen.Width = 5
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          object TeeFunction3: TAverageTeeFunction
          end
        end
        object Series8: TLineSeries
          HoverElement = [heCurrent]
          Legend.Visible = False
          DataSource = Series1
          SeriesColor = clGray
          ShowInLegend = False
          Brush.BackColor = clDefault
          LinePen.Width = 5
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          object TeeFunction4: TAverageTeeFunction
          end
        end
      end
      object Chart3: TChart
        Left = 4
        Top = 384
        Width = 680
        Height = 181
        BackWall.Brush.Gradient.Direction = gdBottomTop
        BackWall.Brush.Gradient.EndColor = clWhite
        BackWall.Brush.Gradient.StartColor = 15395562
        BackWall.Brush.Gradient.Visible = True
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = 15395562
        Gradient.Visible = True
        LeftWall.Color = 14745599
        Legend.CheckBoxes = True
        Legend.Font.Name = 'Verdana'
        Legend.LegendStyle = lsSeries
        Legend.Shadow.Transparency = 0
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        OnClickSeries = Chart3ClickSeries
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.LabelsFormat.Font.Name = 'Verdana'
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Axis.Color = 4210752
        LeftAxis.AxisValuesFormat = '00e-0'
        LeftAxis.Grid.Color = 11119017
        LeftAxis.LabelsExponent = True
        LeftAxis.LabelsFormat.Font.Name = 'Verdana'
        LeftAxis.Logarithmic = True
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Caption = 'Nobjects'
        LeftAxis.Title.Font.Name = 'Verdana'
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.LabelsFormat.Font.Name = 'Verdana'
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.LabelsFormat.Font.Name = 'Verdana'
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        Align = alClient
        TabOrder = 1
        Visible = False
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object PointSeries1: TPointSeries
          HoverElement = [heCurrent]
          Title = '0-1'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries2: TPointSeries
          HoverElement = [heCurrent]
          Title = '1-2'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries3: TPointSeries
          HoverElement = [heCurrent]
          Title = '2-3'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries4: TPointSeries
          HoverElement = [heCurrent]
          Title = '3-4'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries5: TPointSeries
          HoverElement = [heCurrent]
          Title = '4-5'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries6: TPointSeries
          HoverElement = [heCurrent]
          Title = '5-6'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries7: TPointSeries
          HoverElement = [heCurrent]
          Title = '6-7'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries8: TPointSeries
          HoverElement = [heCurrent]
          Title = '7-8'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries9: TPointSeries
          HoverElement = [heCurrent]
          Title = '8-9'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries10: TPointSeries
          HoverElement = [heCurrent]
          Title = '9-10'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object PointSeries11: TPointSeries
          HoverElement = [heCurrent]
          Title = '10-11'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart4: TChart
        Left = 4
        Top = 206
        Width = 680
        Height = 178
        BackWall.Brush.Style = bsClear
        BackWall.Brush.Gradient.Direction = gdBottomTop
        BackWall.Brush.Gradient.EndColor = clWhite
        BackWall.Brush.Gradient.StartColor = 15395562
        BackWall.Brush.Gradient.Visible = True
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = clSilver
        Gradient.Visible = True
        LeftWall.Color = 14745599
        Legend.ColorWidth = 30
        Legend.Font.Name = 'Verdana'
        Legend.Shadow.Transparency = 0
        Legend.Symbol.Width = 30
        MarginRight = 6
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.LabelsFormat.Font.Name = 'Verdana'
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        Chart3DPercent = 1
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Axis.Color = 4210752
        LeftAxis.Grid.Color = 11119017
        LeftAxis.LabelsFormat.Font.Name = 'Verdana'
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Caption = 'Layers'
        LeftAxis.Title.Font.Name = 'Verdana'
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.LabelsFormat.Font.Name = 'Verdana'
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.LabelsFormat.Font.Name = 'Verdana'
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        View3DWalls = False
        Zoom.Animated = True
        Align = alTop
        TabOrder = 3
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Label23: TLabel
          Left = 232
          Top = 191
          Width = 34
          Height = 16
          Caption = 'Green'
        end
        object Label24: TLabel
          Left = 54
          Top = 191
          Width = 22
          Height = 16
          Caption = 'Red'
        end
        object Label25: TLabel
          Left = 551
          Top = 191
          Width = 92
          Height = 16
          Caption = 'Sub-populations'
        end
        object Label26: TLabel
          Left = 405
          Top = 191
          Width = 24
          Height = 16
          Caption = 'Blue'
        end
        object CheckListBox1: TCheckListBox
          Left = 560
          Top = 85
          Width = 73
          Height = 71
          Items.Strings = (
            'Red'
            'Green'
            'Blue'
            'Fraction')
          TabOrder = 0
          OnClick = CheckBox2Click
        end
        object Series20: THorizBarSeries
          HoverElement = []
          BarBrush.Gradient.Direction = gdLeftRight
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'Red'
          Gradient.Direction = gdLeftRight
          MultiBar = mbStacked100
          XValues.Name = 'Bar'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
        object Series21: THorizBarSeries
          HoverElement = []
          BarBrush.Gradient.Direction = gdLeftRight
          Marks.Visible = False
          SeriesColor = clLime
          Title = 'Green'
          Gradient.Direction = gdLeftRight
          MultiBar = mbStacked100
          XValues.Name = 'Bar'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
        object Series22: THorizBarSeries
          HoverElement = []
          BarBrush.Gradient.Direction = gdLeftRight
          Marks.Visible = False
          SeriesColor = clAqua
          Title = 'Blue'
          Gradient.Direction = gdLeftRight
          MultiBar = mbStacked100
          XValues.Name = 'Bar'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
      end
      object Chart5: TChart
        Left = 4
        Top = 21
        Width = 680
        Height = 186
        BackWall.Brush.Style = bsClear
        BackWall.Brush.Gradient.Direction = gdBottomTop
        BackWall.Brush.Gradient.EndColor = clWhite
        BackWall.Brush.Gradient.StartColor = 15395562
        BackWall.Brush.Gradient.Visible = True
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = clSilver
        Gradient.Visible = True
        LeftWall.Color = 14745599
        Legend.ColorWidth = 30
        Legend.Font.Name = 'Verdana'
        Legend.Shadow.Transparency = 0
        Legend.Symbol.Width = 30
        MarginRight = 6
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.LabelsFormat.Font.Name = 'Verdana'
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        Chart3DPercent = 1
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Axis.Color = 4210752
        LeftAxis.Grid.Color = 11119017
        LeftAxis.LabelsFormat.Font.Name = 'Verdana'
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Caption = 'Layers'
        LeftAxis.Title.Font.Name = 'Verdana'
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.LabelsFormat.Font.Name = 'Verdana'
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.LabelsFormat.Font.Name = 'Verdana'
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        View3DWalls = False
        Zoom.Animated = True
        Align = alBottom
        TabOrder = 4
        Visible = False
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Label27: TLabel
          Left = 232
          Top = 191
          Width = 34
          Height = 16
          Caption = 'Green'
        end
        object Label28: TLabel
          Left = 54
          Top = 191
          Width = 22
          Height = 16
          Caption = 'Red'
        end
        object Label29: TLabel
          Left = 551
          Top = 191
          Width = 92
          Height = 16
          Caption = 'Sub-populations'
        end
        object Label30: TLabel
          Left = 405
          Top = 191
          Width = 24
          Height = 16
          Caption = 'Blue'
        end
        object HorizBarSeries1: THorizBarSeries
          HoverElement = []
          BarBrush.Gradient.Direction = gdLeftRight
          Marks.Visible = False
          SeriesColor = clSilver
          Title = 'Fraction'
          Gradient.Direction = gdLeftRight
          MultiBar = mbStacked
          XValues.Name = 'Bar'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loAscending
        end
      end
      object Chart2: TChart
        Left = 4
        Top = 27
        Width = 680
        Height = 179
        BackWall.Brush.Gradient.Direction = gdBottomTop
        BackWall.Brush.Gradient.EndColor = clWhite
        BackWall.Brush.Gradient.StartColor = 15395562
        BackWall.Brush.Gradient.Visible = True
        BackWall.Transparent = False
        Foot.Font.Color = clBlue
        Foot.Font.Name = 'Verdana'
        Gradient.Direction = gdBottomTop
        Gradient.EndColor = clWhite
        Gradient.MidColor = 15395562
        Gradient.StartColor = 15395562
        Gradient.Visible = True
        LeftWall.Color = 14745599
        Legend.CheckBoxes = True
        Legend.Font.Name = 'Verdana'
        Legend.LegendStyle = lsSeries
        Legend.Shadow.Transparency = 0
        RightWall.Color = 14745599
        Title.Font.Name = 'Verdana'
        Title.Text.Strings = (
          'TChart')
        OnClickSeries = Chart2ClickSeries
        BottomAxis.Axis.Color = 4210752
        BottomAxis.Grid.Color = 11119017
        BottomAxis.LabelsFormat.Font.Name = 'Verdana'
        BottomAxis.TicksInner.Color = 11119017
        BottomAxis.Title.Font.Name = 'Verdana'
        DepthAxis.Axis.Color = 4210752
        DepthAxis.Grid.Color = 11119017
        DepthAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthAxis.TicksInner.Color = 11119017
        DepthAxis.Title.Font.Name = 'Verdana'
        DepthTopAxis.Axis.Color = 4210752
        DepthTopAxis.Grid.Color = 11119017
        DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
        DepthTopAxis.TicksInner.Color = 11119017
        DepthTopAxis.Title.Font.Name = 'Verdana'
        LeftAxis.Axis.Color = 4210752
        LeftAxis.AxisValuesFormat = '00e-0'
        LeftAxis.Grid.Color = 11119017
        LeftAxis.LabelsExponent = True
        LeftAxis.LabelsFormat.Font.Name = 'Verdana'
        LeftAxis.Logarithmic = True
        LeftAxis.TicksInner.Color = 11119017
        LeftAxis.Title.Caption = 'Npixels'
        LeftAxis.Title.Font.Name = 'Verdana'
        RightAxis.Axis.Color = 4210752
        RightAxis.Grid.Color = 11119017
        RightAxis.LabelsFormat.Font.Name = 'Verdana'
        RightAxis.TicksInner.Color = 11119017
        RightAxis.Title.Font.Name = 'Verdana'
        TopAxis.Axis.Color = 4210752
        TopAxis.Grid.Color = 11119017
        TopAxis.LabelsFormat.Font.Name = 'Verdana'
        TopAxis.TicksInner.Color = 11119017
        TopAxis.Title.Font.Name = 'Verdana'
        View3D = False
        Align = alTop
        TabOrder = 5
        Visible = False
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series9: TPointSeries
          HoverElement = [heCurrent]
          Title = '0-1'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series10: TPointSeries
          HoverElement = [heCurrent]
          Title = '1-2'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series11: TPointSeries
          HoverElement = [heCurrent]
          Title = '2-3'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series12: TPointSeries
          HoverElement = [heCurrent]
          Title = '3-4'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series13: TPointSeries
          HoverElement = [heCurrent]
          Title = '4-5'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series14: TPointSeries
          HoverElement = [heCurrent]
          Title = '5-6'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series15: TPointSeries
          HoverElement = [heCurrent]
          Title = '6-7'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series16: TPointSeries
          HoverElement = [heCurrent]
          Title = '7-8'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series17: TPointSeries
          HoverElement = [heCurrent]
          Title = '8-9'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series18: TPointSeries
          HoverElement = [heCurrent]
          Title = '9-10'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series19: TPointSeries
          HoverElement = [heCurrent]
          Title = '10-11'
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
  end
end
