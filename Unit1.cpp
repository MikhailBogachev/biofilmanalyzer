//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CGAUGES"
#pragma link "PERFGRAP"
#pragma link "cgauges"
#pragma resource "*.dfm"
TForm1 *Form1;
TBitmap *pBitmap, *pBitmap2, *pBitmap3;
TRGBTriple **cBitmap1;
unsigned long **cBitmap2;
unsigned long *cell_sizes, *cell_g_sizes, *cell_r_sizes, *cell_b_sizes;
double green, green_pixels, red, red_pixels, blue, blue_pixels, ratio, aver_r_cell_size, aver_g_cell_size, aver_b_cell_size, nom, denom;
unsigned long h, w, cells, cells_g, cells_r, cells_b, min_cell_size, max_cell_size;
wchar_t path[300];

void segm_scan (void)
{
 TRGBTriple ptr;
 int i,j,cnt,cnt_r,cnt_g,cnt_b,flag,ii,jj;
 unsigned long ptr2, ptr3, ptr4, ptr5, ptr6, getmin, ptrx;
 double ptr1;

 /*     Selecting and counting whatever cells in a single-channel image       */

  for (i=0;i<h;++i)
  {
   for (j=0;j<w;++j)
   {
	ptr = cBitmap1[i][j];
	if (!(Form1->RadioGroup3->ItemIndex))
	{
	 if (!(Form1->RadioGroup2->ItemIndex))
	  ptr1 = (double)ptr.rgbtRed - (double)ptr.rgbtGreen;
	 else if (Form1->RadioGroup2->ItemIndex == 1)
	  ptr1 = (double)ptr.rgbtGreen - (double)ptr.rgbtRed;
	}
	else if (Form1->RadioGroup3->ItemIndex == 1)
	{
	 if (!(Form1->RadioGroup2->ItemIndex))
	  ptr1 = (double)ptr.rgbtRed;
	 else if (Form1->RadioGroup2->ItemIndex == 1)
	  ptr1 = (double)ptr.rgbtGreen;
	 else
	  ptr1 = (double)ptr.rgbtBlue;
	}
	else
	 ptr1 = 0.299*(double)ptr.rgbtRed + 0.587*(double)ptr.rgbtGreen + 0.114*(double)ptr.rgbtBlue;

	if (ptr1 > (double)Form1->TrackBar1->Position)
	{
	 green_pixels += 1.0;
	 red_pixels += 1.0;
	 blue_pixels += 1.0;

	 if (j)
	  ptr3 = cBitmap2[i][j-1];
	 if (i)
	  ptr4 = cBitmap2[i-1][j];

     if (ptr3 < 1)
      if (ptr4 < 1)
       ++cells;

	 ptr2 = cells;
	 cBitmap2[i][j] = ptr2;
	 cell_sizes[cells]++;

     if (i)
     {
      if (ptr3)
       if (ptr3 < ptr2)
        ptr2 = ptr3;

      if (ptr4)
	   if (ptr4 < ptr2)
        ptr2 = ptr4;

      if (ptr3 || ptr4)
	  {
       if ((ptr3 < cells) || (ptr4 < cells))
	   {
		cBitmap2[i][j] = ptr2;
		cell_sizes[cells]--;
		cell_sizes[ptr2]++;

		ii = i;
		jj = j;
		while (cell_sizes[cells])
		{
		 ptrx = cBitmap2[ii][jj];
		 if (ptrx == cells)
		 {
		  cBitmap2[ii][jj] = ptr2;
          cell_sizes[ptrx]--;
          cell_sizes[ptr2]++;
		 }

         --jj;
		 if ((jj <= 0) || (ptrx < 1))
          break;
        }
	   }
      }
	 }
    }

 /*     Applying transparency to the background noise in a single-channel image       */

    else
    {
	 ptr.rgbtGreen = ptr.rgbtRed = ptr.rgbtBlue = 0;
	 cBitmap1[i][j] = ptr;
	}
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();

 if (!(Form1->CheckBox1->Checked))
 {
  if (Form1->ComboBox1->ItemIndex > 0)
  {

 /*     Reverse horizontal cycle        --      2nd pass    */

  for (i=h-1;i>=0;--i)
  {
   for (j=w-1;j>=0;--j)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
     if (j < w-1)
	  ptr3 = cBitmap2[i][j+1];
     else
	  ptr3 = 0;

     if (i < h-1)
	  ptr4 = cBitmap2[i+1][j];
	 else
	  ptr4 = 0;

	 if (ptr3)
      if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
       ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr4)
      if (ptr4 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
       ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
      }
	}
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();

 /*     Once more direct horizontal cycle   --      3rd pass    */

  for (i=0;i<h;++i)
  {
   for (j=0;j<w;++j)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
	 if (j)
	  ptr3 = cBitmap2[i][j-1];
	 else
	  ptr3 = 0;

     if (i)
	  ptr4 = cBitmap2[i-1][j];
	 else
	  ptr4 = 0;

     if (ptr3)
      if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
	   ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
	  }

	 if (ptr4)
      if (ptr4 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
	   ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
	  }
	}
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();
 }

 if (Form1->ComboBox1->ItemIndex > 1)
 {

 /*     Reverse vertical cycle        --      4th pass    */

  for (j=w-1;j>=0;--j)
  {
   for (i=h-1;i>=0;--i)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
     if (i < h-1)
	  ptr3 = cBitmap2[i+1][j];
	 else
	  ptr3 = 0;

     if (j < w-1)
	  ptr4 = cBitmap2[i][j+1];
     else
	  ptr4 = 0;

     if (i)
	  ptr5 = cBitmap2[i-1][j];
     else
	  ptr5 = 0;

	 if (j)
	  ptr6 = cBitmap2[i][j-1];
     else
	  ptr6 = 0;

     if (ptr3)
	  if (ptr3 < ptr2)
      {
	   cell_sizes[ptr2]--;
	   cell_sizes[ptr3]++;
       ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
	  }

     if (ptr4)
      if (ptr4 < ptr2)
      {
	   cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
	   ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
	  }

     if (ptr5)
      if (ptr5 < ptr2)
      {
       cell_sizes[ptr2]--;
	   cell_sizes[ptr5]++;
       ptr2 = ptr5;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr6)
      if (ptr6 < ptr2)
      {
       cell_sizes[ptr2]--;
	   cell_sizes[ptr6]++;
       ptr2 = ptr6;
	   cBitmap2[i][j] = ptr2;
	  }
    }
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();

 /*     Direct vertical cycle   --      5th pass    */

  for (j=0;j<w;++j)
  {
   for (i=0;i<h;++i)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
     if (i)
	  ptr3 = cBitmap2[i-1][j];
     else
	  ptr3 = 0;

	 if (j)
	  ptr4 = cBitmap2[i][j-1];
     else
	  ptr4 = 0;

     if (i < h-1)
	  ptr5 = cBitmap2[i+1][j];
     else
	  ptr5 = 0;

     if (j < w-1)
	  ptr6 = cBitmap2[i][j+1];
	 else
	  ptr6 = 0;

     if (ptr3)
      if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
       ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr4)
      if (ptr4 < ptr2)
	  {
       cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
       ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
	  }

     if (ptr5)
	  if (ptr5 < ptr2)
      {
       cell_sizes[ptr2]--;
	   cell_sizes[ptr5]++;
       ptr2 = ptr5;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr6)
	  if (ptr6 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr6]++;
	   ptr2 = ptr6;
	   cBitmap2[i][j] = ptr2;
	  }
	}
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();

 }


 if (Form1->ComboBox1->ItemIndex > 2)
 {

 /*     Reverse vertical cycle        --      6th pass    */

  for (j=w-1;j>=0;--j)
  {
   for (i=h-1;i>=0;--i)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
     if (i < h-1)
	  ptr3 = cBitmap2[i+1][j];
     else
	  ptr3 = 0;

     if (j < w-1)
	  ptr4 = cBitmap2[i][j+1];
     else
	  ptr4 = 0;

     if (i)
	  ptr5 = cBitmap2[i-1][j];
     else
	  ptr5 = 0;

     if (j)
	  ptr6 = cBitmap2[i][j-1];
     else
	  ptr6 = 0;

     if (ptr3)
      if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
       ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr4)
      if (ptr4 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
       ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
	  }

     if (ptr5)
      if (ptr5 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr5]++;
	   ptr2 = ptr5;
	   cBitmap2[i][j] = ptr2;
	  }

     if (ptr6)
      if (ptr6 < ptr2)
      {
	   cell_sizes[ptr2]--;
       cell_sizes[ptr6]++;
       ptr2 = ptr6;
	   cBitmap2[i][j] = ptr2;
      }
    }
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();

 /*     Direct vertical cycle   --      7th pass    */

  for (j=0;j<w;++j)
  {
   for (i=0;i<h;++i)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
	{
     if (i)
	  ptr3 = cBitmap2[i-1][j];
	 else
	  ptr3 = 0;

	 if (j)
	  ptr4 = cBitmap2[i][j-1];
     else
	  ptr4 = 0;

     if (i < h-1)
	  ptr5 = cBitmap2[i+1][j];
	 else
	  ptr5 = 0;

     if (j < w-1)
	  ptr6 = cBitmap2[i][j+1];
     else
	  ptr6 = 0;

     if (ptr3)
	  if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
       ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr4)
      if (ptr4 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
       ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr5)
      if (ptr5 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr5]++;
	   ptr2 = ptr5;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr6)
      if (ptr6 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr6]++;
	   ptr2 = ptr6;
	   cBitmap2[i][j] = ptr2;
	  }
    }
   }

  }

   Form1->CGauge1->Progress++;
   Form1->CGauge1->Refresh();
 }


 if (Form1->ComboBox1->ItemIndex > 3)
 {

 /*     Reverse vertical cycle        --      8th pass    */

  for (j=w-1;j>=0;--j)
  {
   for (i=h-1;i>=0;--i)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
	 if (i < h-1)
	  ptr3 = cBitmap2[i+1][j];
     else
	  ptr3 = 0;

     if (j < w-1)
	  ptr4 = cBitmap2[i][j+1];
     else
	  ptr4 = 0;

	 if (i)
	  ptr5 = cBitmap2[i-1][j];
	 else
	  ptr5 = 0;

	 if (j)
	  ptr6 = cBitmap2[i][j-1];
     else
      ptr6 = 0;

     if (ptr3)
      if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
       ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr4)
      if (ptr4 < ptr2)
      {
	   cell_sizes[ptr2]--;
       cell_sizes[ptr4]++;
       ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr5)
      if (ptr5 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr5]++;
       ptr2 = ptr5;
	   cBitmap2[i][j] = ptr2;
      }

	 if (ptr6)
      if (ptr6 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr6]++;
       ptr2 = ptr6;
	   cBitmap2[i][j] = ptr2;
	  }
	}
   }

  }

  Form1->CGauge1->Progress++;
  Form1->CGauge1->Refresh();

 /*     Direct vertical cycle   --      9th pass    */

  for (j=0;j<w;++j)
  {
   for (i=0;i<h;++i)
   {
	ptr2 = cBitmap2[i][j];

    if (ptr2)
    {
     if (i)
	  ptr3 = cBitmap2[i-1][j];
     else
	  ptr3 = 0;

     if (j)
	  ptr4 = cBitmap2[i][j-1];
     else
	  ptr4 = 0;

	 if (i < h-1)
	  ptr5 = cBitmap2[i+1][j];
	 else
	  ptr5 = 0;

     if (j < w-1)
	  ptr6 = cBitmap2[i][j+1];
     else
	  ptr6 = 0;

     if (ptr3)
      if (ptr3 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr3]++;
	   ptr2 = ptr3;
	   cBitmap2[i][j] = ptr2;
	  }

	 if (ptr4)
	  if (ptr4 < ptr2)
	  {
	   cell_sizes[ptr2]--;
	   cell_sizes[ptr4]++;
	   ptr2 = ptr4;
	   cBitmap2[i][j] = ptr2;
	  }

	 if (ptr5)
	  if (ptr5 < ptr2)
	  {
       cell_sizes[ptr2]--;
       cell_sizes[ptr5]++;
	   ptr2 = ptr5;
	   cBitmap2[i][j] = ptr2;
      }

     if (ptr6)
      if (ptr6 < ptr2)
      {
       cell_sizes[ptr2]--;
       cell_sizes[ptr6]++;
       ptr2 = ptr6;
       cBitmap2[i][j] = ptr2;
      }
	}
   }

   }

   Form1->CGauge1->Progress++;
   Form1->CGauge1->Refresh();

  }
 }
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
		: TForm(Owner)
{
 DragAcceptFiles(Handle, true);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::WMDropFiles( TWMDropFiles &Message)
{
 FILE *oz;
 char *fname;
 if (pBitmap) delete (pBitmap);
 if (pBitmap2) delete (pBitmap2);
 if (pBitmap3) delete (pBitmap3);
 Graphics::TBitmap *pBitmap = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap2 = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap3 = new Graphics::TBitmap();
 unsigned long ptr2;
 int i,j,k,cnt,cnt_r,cnt_g,cnt_b,flag,ii,jj;
 HDROP drop=(HDROP)Message.Drop;
 UINT nFiles = DragQueryFile(drop,0xFFFFFFFF,NULL,MAX_PATH);
 CGauge1->MaxValue = nFiles * (ComboBox1->ItemIndex * 2 + 1);
 CGauge1->Progress = 0;

 for(int k=0;k<nFiles;k++)
 {
  green_pixels = 0.0;
  red_pixels = 0.0;
  blue_pixels = 0.0;
  cells = 0;
  cells_g = 0;
  cells_r = 0;
  cells_b = 0;

  DragQueryFile(drop,k,path,sizeof(path));
  Image4->Picture->LoadFromFile(AnsiString(path));
  Image4->Refresh();

 pBitmap->Assign(Form1->Image4->Picture->Graphic);
 pBitmap2->Assign(Form1->Image4->Picture->Graphic);
 pBitmap3->Assign(Form1->Image4->Picture->Graphic);

 h = pBitmap->Height;
 w = pBitmap->Width;

 pBitmap->PixelFormat = pf24bit;
 pBitmap2->PixelFormat = pf32bit;
 pBitmap3->PixelFormat = pf32bit;

 pBitmap2->Canvas->Brush->Color = (TColor)0;
 pBitmap2->Canvas->FillRect(Rect(0,0,w,h));

 if (cBitmap1) free (cBitmap1);
 if (cBitmap2) free (cBitmap2);
 cBitmap1 = (TRGBTriple**) calloc (h, sizeof (TRGBTriple*));
 cBitmap2 = (unsigned long**) calloc (h, sizeof (unsigned long*));
 for (i=0;i<h;++i)
 {
  cBitmap1[i] = (TRGBTriple*) pBitmap->ScanLine[i];
  cBitmap2[i] = (unsigned long*) pBitmap2->ScanLine[i];
 }

 fname = (char*) calloc (1,1000);
 if (cell_sizes) free(cell_sizes);
 if (cell_g_sizes) free(cell_g_sizes);
 if (cell_r_sizes) free(cell_r_sizes);
 if (cell_b_sizes) free(cell_b_sizes);
 cell_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_g_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_r_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_b_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));

  segm_scan();

  Image1->Picture->Bitmap = pBitmap;
  Image1->Refresh();

  if (!(TrackBar2->Position))
  {
   TrackBar2->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar2->Frequency = 1;
  }

  if (TrackBar3->Position == TrackBar3->Max)
  {
   TrackBar3->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar3->Frequency = 1;
   TrackBar3->Position = (int)(log((double)(h*w))/log(1.001));
  }

  if (!(TrackBar5->Position))
  {
   TrackBar5->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar5->Frequency = 1;
   TrackBar5->Position = (int)(log(Edit17->Text.ToDouble())/log(1.001));
  }

  if (!(TrackBar4->Position))
  {
   TrackBar4->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar4->Frequency = 1;
   TrackBar4->Position = (int)(log(Edit16->Text.ToDouble())/log(1.001));
   TrackBar5->Position = (int)(log(Edit16->Text.ToDouble()*10)/log(1.001));
  }

  min_cell_size = (long)pow(1.001,(double)TrackBar2->Position);
  max_cell_size = (long)pow(1.001,(double)TrackBar3->Position);

  pBitmap3->Canvas->CopyRect(Rect(0,0,w,h),pBitmap2->Canvas,Rect(0,0,w,h));
  Image3->Picture->Bitmap = pBitmap3;


  switch (Form1->RadioGroup2->ItemIndex)
  {
   case 1:

 /*     Applying cell size thresholds and calculating cell size statistics for green cells   */

		 if (!(CheckBox1->Checked))
		 {
		  cells_g = 0;
		  aver_g_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_g_sizes[cells_g] = cell_sizes[i];
			  aver_g_cell_size += cell_g_sizes[cells_g];
			  cells_g++;
			 }
		  }
		  if (cells_g)
		   aver_g_cell_size /= (double)cells_g;
		  else
           aver_g_cell_size = 0.0;

		  Edit1->Text = FloatToStr(green_pixels);
		  Edit2->Text = FloatToStr((double)cells_g);
		  Edit3->Text = FloatToStr(aver_g_cell_size);

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();
		 }

		 else
		 {
		  Edit1->Text = FloatToStr(green_pixels);
		  aver_g_cell_size = Edit3->Text.ToDouble();
		 }

		  if (aver_g_cell_size)
		   Edit4->Text = FloatToStr(green_pixels/aver_g_cell_size);
		  else
		   Edit4->Text = "";

		  if (aver_g_cell_size)
		   Form1->StringGrid3->Cells[Form1->StringGrid3->Col][Form1->StringGrid3->Row+k] = FloatToStrF(green_pixels/aver_g_cell_size,ffFixed,4,3);
		  break;

   case 0:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		 if (!(CheckBox1->Checked))
		 {
		  cells_r = 0;
		  aver_r_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_r_sizes[cells_r] = cell_sizes[i];
			  aver_r_cell_size += cell_r_sizes[cells_r];
			  cells_r++;
			 }
		  }
		  if (cells_r)
		   aver_r_cell_size /= (double)cells_r;
		  else
		   aver_r_cell_size = 0.0;

		  Edit5->Text = FloatToStr(red_pixels);
		  Edit6->Text = FloatToStr((double)cells_r);
		  Edit7->Text = FloatToStr(aver_r_cell_size);

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();
		 }

		 else
		 {
		  Edit5->Text = FloatToStr(red_pixels);
		  aver_r_cell_size = Edit7->Text.ToDouble();
		 }
		  if (aver_r_cell_size)
		   Edit8->Text = FloatToStr(red_pixels/aver_r_cell_size);
		  else
		   Edit8->Text = "";

		  if (aver_r_cell_size)
		   Form1->StringGrid4->Cells[Form1->StringGrid4->Col][Form1->StringGrid4->Row+k] = FloatToStrF(red_pixels/aver_r_cell_size,ffFixed,4,3);
		  break;

   case 2:

 /*     Applying cell size thresholds and calculating cell size statistics for blue cells   */

		 if (!(CheckBox1->Checked))
		 {
		  cells_b = 0;
		  aver_b_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_b_sizes[cells_b] = cell_sizes[i];
			  aver_b_cell_size += cell_b_sizes[cells_b];
			  cells_b++;
			 }
		  }
		  if (cells_b)
		   aver_b_cell_size /= (double)cells_b;
		  else
		   aver_b_cell_size = 0.0;

		  Edit12->Text = FloatToStr(blue_pixels);
		  Edit13->Text = FloatToStr((double)cells_b);
		  Edit14->Text = FloatToStr(aver_b_cell_size);

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();
		 }

		 else
		 {
		  Edit12->Text = FloatToStr(blue_pixels);
		  aver_b_cell_size = Edit14->Text.ToDouble();
         }

		  if (aver_b_cell_size)
		   Edit15->Text = FloatToStr(blue_pixels/aver_b_cell_size);
		  else
		   Edit15->Text = "";

		  if (aver_b_cell_size)
		   Form1->StringGrid7->Cells[Form1->StringGrid7->Col][Form1->StringGrid7->Row+k] = FloatToStrF(blue_pixels/aver_b_cell_size,ffFixed,4,3);
  }
 }

 for (i=0;i<8;++i)
  Chart1->Series[i]->Clear();

 for (i=0;i<3;++i)
  Chart4->Series[i]->Clear();
  Chart5->Series[0]->Clear();

 for (j=0;j<7;++j)
 {
  green = 0.0;
  red = 0.0;
  blue = 0.0;
  ratio = 0.0;
  cnt = 0; cnt_r = 0; cnt_g = 0; cnt_b = 0;

  for (i=0;i<100;++i)
  {
   nom = 0;
   denom = 0;

   /*   Calculating sub-population	*/

   if (CheckBox2->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox4->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox6->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (CheckBox3->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox5->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox7->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (denom > 0)
	Form1->StringGrid1->Cells[j][i] = nom / denom;

	/*  Plotting graphs   */

   if (CheckListBox1->Checked[0])
   if (!(Form1->StringGrid2->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[0]->AddXY(StringGrid2->Cells[j][0].ToDouble(),Form1->StringGrid4->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[0]->AddXY(StringGrid4->Cells[j][i].ToDouble(),(double)(i+1));
	 red += Form1->StringGrid4->Cells[j][i].ToDouble();
	 cnt_r++;
	}

   if (CheckListBox1->Checked[1])
   if (!(Form1->StringGrid5->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[1]->AddXY(StringGrid5->Cells[j][0].ToDouble(),Form1->StringGrid3->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[1]->AddXY(StringGrid3->Cells[j][i].ToDouble(),(double)(i+1));
	 green += Form1->StringGrid3->Cells[j][i].ToDouble();
	 cnt_g++;
	}

   if (CheckListBox1->Checked[2])
   if (!(Form1->StringGrid6->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[2]->AddXY(StringGrid6->Cells[j][0].ToDouble(),Form1->StringGrid7->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[2]->AddXY(StringGrid7->Cells[j][i].ToDouble(),(double)(i+1));
	 blue += Form1->StringGrid7->Cells[j][i].ToDouble();
	 cnt_b++;
	}

   if (CheckListBox1->Checked[3])
   if (!(Form1->StringGrid8->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid1->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[3]->AddXY(StringGrid8->Cells[j][0].ToDouble(),Form1->StringGrid1->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart5->Series[0]->AddXY(StringGrid1->Cells[j][i].ToDouble(),(double)(i+1));
	 ratio += Form1->StringGrid1->Cells[j][i].ToDouble();
	 cnt++;
	}
  }

  if (CheckListBox1->Checked[0])
  if (cnt_r)
   Chart1->Series[4]->AddXY(StringGrid2->Cells[j][0].ToDouble(),red /(double)cnt_r);

  if (CheckListBox1->Checked[1])
  if (cnt_g)
   Chart1->Series[5]->AddXY(StringGrid5->Cells[j][0].ToDouble(),green /(double)cnt_g);

  if (CheckListBox1->Checked[2])
  if (cnt_b)
   Chart1->Series[6]->AddXY(StringGrid6->Cells[j][0].ToDouble(),blue /(double)cnt_b);

  if (CheckListBox1->Checked[3])
  if (cnt)
   Chart1->Series[7]->AddXY(StringGrid8->Cells[j][0].ToDouble(),ratio /(double)cnt);
 }

 free(fname);
 DragFinish(drop);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button1Click(TObject *Sender)
{
 FILE *oz;
 char *fname;
 TRGBTriple ptr;
 unsigned long ptr2, ptr3, ptr4, ptr5, ptr6, getmin, ptrx;
 double ptr1;
 int i,j,cnt,cnt_r,cnt_g,cnt_b,flag,ii,jj;

 if (pBitmap) delete (pBitmap);
 if (pBitmap2) delete (pBitmap2);
 if (pBitmap3) delete (pBitmap3);
 Graphics::TBitmap *pBitmap = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap2 = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap3 = new Graphics::TBitmap();

 CGauge1->MaxValue = ComboBox1->ItemIndex * 2 + 1;
 CGauge1->Progress = 0;

 {
  green_pixels = 0.0;
  red_pixels = 0.0;
  blue_pixels = 0.0;
  cells = 0;
  cells_g = 0;
  cells_r = 0;
  cells_b = 0;

  Image4->Picture->LoadFromFile(AnsiString(path));
  Image4->Refresh();

 pBitmap->Assign(Form1->Image4->Picture->Graphic);
 pBitmap2->Assign(Form1->Image4->Picture->Graphic);
 pBitmap3->Assign(Form1->Image4->Picture->Graphic);

 h = pBitmap->Height;
 w = pBitmap->Width;

 pBitmap->PixelFormat = pf24bit;
 pBitmap2->PixelFormat = pf32bit;
 pBitmap3->PixelFormat = pf32bit;

 pBitmap2->Canvas->Brush->Color = (TColor)0;
 pBitmap2->Canvas->FillRect(Rect(0,0,w,h));

 if (cBitmap1) free (cBitmap1);
 if (cBitmap2) free (cBitmap2);
 cBitmap1 = (TRGBTriple**) calloc (h, sizeof (TRGBTriple*));
 cBitmap2 = (unsigned long**) calloc (h, sizeof (unsigned long*));
 for (i=0;i<h;++i)
 {
  cBitmap1[i] = (TRGBTriple*) pBitmap->ScanLine[i];
  cBitmap2[i] = (unsigned long*) pBitmap2->ScanLine[i];
 }

 fname = (char*) calloc (1,1000);
 if (cell_sizes) free(cell_sizes);
 if (cell_g_sizes) free(cell_g_sizes);
 if (cell_r_sizes) free(cell_r_sizes);
 if (cell_b_sizes) free(cell_b_sizes);
 cell_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_g_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_r_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_b_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));

 segm_scan();

  Image1->Picture->Bitmap = pBitmap;
  Image1->Refresh();

  if (!(TrackBar2->Position))
  {
   TrackBar2->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar2->Frequency = 1;
  }

  if (TrackBar3->Position == TrackBar3->Max)
  {
   TrackBar3->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar3->Frequency = 1;
   TrackBar3->Position = (int)(log((double)(h*w))/log(1.001));
  }

  if (!(TrackBar4->Position))
  {
   TrackBar4->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar4->Frequency = 1;
  }

  if (!(TrackBar5->Position))
  {
   TrackBar5->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar5->Frequency = 1;
  }

  min_cell_size = (long)pow(1.001,(double)TrackBar2->Position);
  max_cell_size = (long)pow(1.001,(double)TrackBar3->Position);

  pBitmap3->Canvas->CopyRect(Rect(0,0,w,h),pBitmap2->Canvas,Rect(0,0,w,h));
  Image3->Picture->Bitmap = pBitmap3;


  switch (Form1->RadioGroup2->ItemIndex)
  {
   case 1:

 /*     Applying cell size thresholds and calculating cell size statistics for green cells   */

		 if (!(CheckBox1->Checked))
		 {
		  cells_g = 0;
          aver_g_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
          {
           if (cell_sizes[i])
            if (cell_sizes[i] > min_cell_size)
             if (cell_sizes[i] < max_cell_size)
             {
              cell_g_sizes[cells_g] = cell_sizes[i];
              aver_g_cell_size += cell_g_sizes[cells_g];
              cells_g++;
             }
		  }
		  if (cells_g)
		   aver_g_cell_size /= (double)cells_g;
		  else
		   aver_g_cell_size = 0.0;

		  Edit1->Text = FloatToStr(green_pixels);
		  Edit2->Text = FloatToStr((double)cells_g);
		  Edit3->Text = FloatToStr(aver_g_cell_size);

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();
		 }

		 else
		 {
          Edit1->Text = FloatToStr(green_pixels);
		  aver_g_cell_size = Edit3->Text.ToDouble();
		 }

		  if (aver_g_cell_size)
		   Edit4->Text = FloatToStr(green_pixels/aver_g_cell_size);
		  else
		   Edit4->Text = "";

		  if (aver_g_cell_size)
		   Form1->StringGrid3->Cells[Form1->StringGrid3->Col][Form1->StringGrid3->Row] = FloatToStrF(green_pixels/aver_g_cell_size,ffFixed,4,3);
          break;

   case 0:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		 if (!(CheckBox1->Checked))
		 {
		  cells_r = 0;
          aver_r_cell_size = 0.0;
          for (i=0; i<h*w*6; ++i)
          {
           if (cell_sizes[i])
            if (cell_sizes[i] > min_cell_size)
             if (cell_sizes[i] < max_cell_size)
             {
              cell_r_sizes[cells_r] = cell_sizes[i];
              aver_r_cell_size += cell_r_sizes[cells_r];
              cells_r++;
             }
          }
		  if (cells_r)
		   aver_r_cell_size /= (double)cells_r;
		  else
		   aver_r_cell_size = 0.0;

		  Edit5->Text = FloatToStr(red_pixels);
		  Edit6->Text = FloatToStr((double)cells_r);
		  Edit7->Text = FloatToStr(aver_r_cell_size);

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();
		 }

		 else
		 {
		  Edit5->Text = FloatToStr(red_pixels);
		  aver_r_cell_size = Edit7->Text.ToDouble();
		 }

		  if (aver_r_cell_size)
		   Edit8->Text = FloatToStr(red_pixels/aver_r_cell_size);
		  else
		   Edit8->Text = "";

		  if (aver_r_cell_size)
		   Form1->StringGrid4->Cells[Form1->StringGrid4->Col][Form1->StringGrid4->Row] = FloatToStrF(red_pixels/aver_r_cell_size,ffFixed,4,3);
		  break;

   case 2:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		 if (!(CheckBox1->Checked))
		 {
		  cells_b = 0;
		  aver_b_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_b_sizes[cells_b] = cell_sizes[i];
			  aver_b_cell_size += cell_b_sizes[cells_b];
			  cells_b++;
			 }
		  }
		  if (cells_b)
		   aver_b_cell_size /= (double)cells_b;
		  else
		   aver_b_cell_size = 0.0;

		  Edit12->Text = FloatToStr(blue_pixels);
		  Edit13->Text = FloatToStr((double)cells_b);
		  Edit14->Text = FloatToStr(aver_b_cell_size);

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();
		 }

		 else
		 {
          Edit12->Text = FloatToStr(blue_pixels);
		  aver_b_cell_size = Edit14->Text.ToDouble();
		 }

		  if (aver_b_cell_size)
		   Edit15->Text = FloatToStr(blue_pixels/aver_b_cell_size);
		  else
		   Edit15->Text = "";

		  if (aver_b_cell_size)
		   Form1->StringGrid7->Cells[Form1->StringGrid7->Col][Form1->StringGrid7->Row] = FloatToStrF(blue_pixels/aver_b_cell_size,ffFixed,4,3);
  }
 }

 for (i=0;i<8;++i)
  Chart1->Series[i]->Clear();

 for (i=0;i<3;++i)
  Chart4->Series[i]->Clear();
  Chart5->Series[0]->Clear();

 for (j=0;j<7;++j)
 {
  green = 0.0;
  red = 0.0;
  blue = 0.0;
  ratio = 0.0;
  cnt = 0; cnt_r = 0; cnt_g = 0; cnt_b = 0;

  for (i=0;i<100;++i)
  {
   nom = 0;
   denom = 0;

   /*   Calculating sub-population	*/

   if (CheckBox2->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox4->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox6->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (CheckBox3->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox5->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox7->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (denom > 0)
	Form1->StringGrid1->Cells[j][i] = nom / denom;

	/*  Plotting graphs   */

   if (CheckListBox1->Checked[0])
   if (!(Form1->StringGrid2->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[0]->AddXY(StringGrid2->Cells[j][0].ToDouble(),Form1->StringGrid4->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[0]->AddXY(StringGrid4->Cells[j][i].ToDouble(),(double)(i+1));
	 red += Form1->StringGrid4->Cells[j][i].ToDouble();
	 cnt_r++;
	}

   if (CheckListBox1->Checked[1])
   if (!(Form1->StringGrid5->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[1]->AddXY(StringGrid5->Cells[j][0].ToDouble(),Form1->StringGrid3->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[1]->AddXY(StringGrid3->Cells[j][i].ToDouble(),(double)(i+1));
	 green += Form1->StringGrid3->Cells[j][i].ToDouble();
	 cnt_g++;
	}

   if (CheckListBox1->Checked[2])
   if (!(Form1->StringGrid6->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[2]->AddXY(StringGrid6->Cells[j][0].ToDouble(),Form1->StringGrid7->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[2]->AddXY(StringGrid7->Cells[j][i].ToDouble(),(double)(i+1));
	 blue += Form1->StringGrid7->Cells[j][i].ToDouble();
	 cnt_b++;
	}

   if (CheckListBox1->Checked[3])
   if (!(Form1->StringGrid8->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid1->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[3]->AddXY(StringGrid8->Cells[j][0].ToDouble(),Form1->StringGrid1->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart5->Series[0]->AddXY(StringGrid1->Cells[j][i].ToDouble(),(double)(i+1));
	 ratio += Form1->StringGrid1->Cells[j][i].ToDouble();
	 cnt++;
	}
  }

  if (CheckListBox1->Checked[0])
  if (cnt_r)
   Chart1->Series[4]->AddXY(StringGrid2->Cells[j][0].ToDouble(),red /(double)cnt_r);

  if (CheckListBox1->Checked[1])
  if (cnt_g)
   Chart1->Series[5]->AddXY(StringGrid5->Cells[j][0].ToDouble(),green /(double)cnt_g);

  if (CheckListBox1->Checked[2])
  if (cnt_b)
   Chart1->Series[6]->AddXY(StringGrid6->Cells[j][0].ToDouble(),blue /(double)cnt_b);

  if (CheckListBox1->Checked[3])
  if (cnt)
   Chart1->Series[7]->AddXY(StringGrid8->Cells[j][0].ToDouble(),ratio /(double)cnt);
 }

  free(fname);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
 for (int i=0;i<7;++i)
 {
  StringGrid2->Cells[i][0] = IntToStr(i+1);
  StringGrid5->Cells[i][0] = IntToStr(i+1);
  StringGrid6->Cells[i][0] = IntToStr(i+1);
  StringGrid8->Cells[i][0] = IntToStr(i+1);
 }

 for (int i=0;i<3;++i)
  CheckListBox1->Checked[i] = true;
  CheckListBox1->Checked[3] = false;

 Chart1->Title->Visible = false;
 Chart2->Title->Visible = false;
 Chart3->Title->Visible = false;
 Chart4->Title->Visible = false;
 Chart5->Title->Visible = false;

 Image2->Picture->Bitmap->PixelFormat = pf32bit;
 Image3->Picture->Bitmap->PixelFormat = pf32bit;
}
//---------------------------------------------------------------------------



void __fastcall TForm1::Button2Click(TObject *Sender)
{
 int i,j;
 Variant vVarApp,vVarBooks,vVarBook,vVarSheets,vVarSheet,vVarCell;

 vVarApp=CreateOleObject("Excel.Application");
 vVarApp.OlePropertySet("Visible",true);
 vVarBooks=vVarApp.OlePropertyGet("Workbooks");
 vVarApp.OlePropertySet("SheetsInNewWorkbook",4);
 vVarBooks.OleProcedure("Add");
// vVarApp.OlePropertySet("SheetsInNewWorkbook",4);
 vVarBook=vVarBooks.OlePropertyGet("Item",1);
 vVarSheets=vVarBook.OlePropertyGet("Worksheets");

 vVarSheet = vVarSheets.OlePropertyGet("Item",1);
// vVarSheet.OlePropertySet("Name","Red");
 for (i=0;i<7;++i)
 {
  vVarCell = vVarSheet.OlePropertyGet("Cells", 1, i+1);
  vVarCell.OlePropertySet("Value", WideString(StringGrid2->Cells[i][0]));
  for (j=0;j<100;++j)
  {
   vVarCell = vVarSheet.OlePropertyGet("Cells", j+2, i+1);
   vVarCell.OlePropertySet("Value", WideString(StringGrid4->Cells[i][j]));
  }
 }

 vVarSheet = vVarSheets.OlePropertyGet("Item",2);
// vVarSheet.OlePropertySet("Name","Green");
 for (i=0;i<7;++i)
 {
  vVarCell = vVarSheet.OlePropertyGet("Cells", 1, i+1);
  vVarCell.OlePropertySet("Value", WideString(StringGrid5->Cells[i][0]));
  for (j=0;j<100;++j)
  {
   vVarCell = vVarSheet.OlePropertyGet("Cells", j+2, i+1);
   vVarCell.OlePropertySet("Value", WideString(StringGrid3->Cells[i][j]));
  }
 }

 vVarSheet = vVarSheets.OlePropertyGet("Item",3);
// vVarSheet.OlePropertySet("Name","Blue");
 for (i=0;i<7;++i)
 {
  vVarCell = vVarSheet.OlePropertyGet("Cells", 1, i+1);
  vVarCell.OlePropertySet("Value", WideString(StringGrid6->Cells[i][0]));
  for (j=0;j<100;++j)
  {
   vVarCell = vVarSheet.OlePropertyGet("Cells", j+2, i+1);
   vVarCell.OlePropertySet("Value", WideString(StringGrid7->Cells[i][j]));
  }
 }

 vVarSheet = vVarSheets.OlePropertyGet("Item",4);
// vVarSheet.OlePropertySet("Name","Fraction");
 for (i=0;i<7;++i)
 {
  vVarCell = vVarSheet.OlePropertyGet("Cells", 1, i+1);
  vVarCell.OlePropertySet("Value", WideString(StringGrid8->Cells[i][0]));
  for (j=0;j<100;++j)
  {
   vVarCell = vVarSheet.OlePropertyGet("Cells", j+2, i+1);
   vVarCell.OlePropertySet("Value", WideString(StringGrid1->Cells[i][j]));
  }
 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::StringGrid3Click(TObject *Sender)
{
 RadioGroup2->ItemIndex = 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::StringGrid4Click(TObject *Sender)
{
 RadioGroup2->ItemIndex = 0;
}
//---------------------------------------------------------------------------



void __fastcall TForm1::Button3Click(TObject *Sender)
{
 FILE *oz;
 char *fname;
 unsigned long ptr2;
 int i,j,cnt,cnt_r,cnt_g,cnt_b,flag;

 if (pBitmap2) delete (pBitmap2);
 if (pBitmap3) delete (pBitmap3);
 Graphics::TBitmap *pBitmap2 = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap3 = new Graphics::TBitmap();

 pBitmap2 = Image2->Picture->Bitmap;
 pBitmap3 = Image3->Picture->Bitmap;

 pBitmap2->PixelFormat = pf32bit;
 pBitmap3->PixelFormat = pf32bit;

 h = pBitmap3->Height;
 w = pBitmap3->Width;

 if (cBitmap2) free (cBitmap2);
 cBitmap2 = (unsigned long**) calloc (h, sizeof (unsigned long*));
 for (i=0;i<h;++i)
  cBitmap2[i] = (unsigned long*) pBitmap2->ScanLine[i];
 pBitmap2->Canvas->CopyRect(Rect(0,0,w,h),pBitmap3->Canvas,Rect(0,0,w,h));

 fname = (char*) calloc (1,1000);

 {
  if (!(TrackBar2->Position))
  {
   TrackBar2->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar2->Frequency = 1;
  }

  if (TrackBar3->Position == TrackBar3->Max)
  {
   TrackBar3->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar3->Frequency = 1;
   TrackBar3->Position = (int)(log((double)(h*w))/log(1.001));
  }

  if (!(TrackBar4->Position))
  {
   TrackBar4->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar4->Frequency = 1;
  }

  if (!(TrackBar5->Position))
  {
   TrackBar5->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar5->Frequency = 1;
  }

  min_cell_size = (long)pow(1.001,(double)TrackBar2->Position);
  max_cell_size = (long)pow(1.001,(double)TrackBar3->Position);

  switch (Form1->RadioGroup2->ItemIndex)
  {
   case 1:

 /*     Applying cell size thresholds and calculating cell size statistics for green cells   */

		  cells_g = 0;
		  aver_g_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_g_sizes[cells_g] = cell_sizes[i];
			  aver_g_cell_size += cell_g_sizes[cells_g];
			  cells_g++;
			 }
		  }
		  if (cells_g)
		   aver_g_cell_size /= (double)cells_g;
		  else
		   aver_g_cell_size = 0.0;


		  Edit1->Text = FloatToStr(green_pixels);
		  Edit2->Text = FloatToStr((double)cells_g);
		  Edit3->Text = FloatToStr(aver_g_cell_size);
		  if (aver_g_cell_size)
		   Edit4->Text = FloatToStr(green_pixels/aver_g_cell_size);
		  else
           Edit4->Text = "";

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();

		  if (aver_g_cell_size)
		   Form1->StringGrid3->Cells[Form1->StringGrid3->Col][Form1->StringGrid3->Row] = FloatToStrF(green_pixels/aver_g_cell_size,ffFixed,4,3);
		  break;

   case 0:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		  cells_r = 0;
		  aver_r_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_r_sizes[cells_r] = cell_sizes[i];
			  aver_r_cell_size += cell_r_sizes[cells_r];
			  cells_r++;
			 }
		  }
		  if (cells_r)
		   aver_r_cell_size /= (double)cells_r;
		  else
		   aver_r_cell_size = 0.0;

		  Edit5->Text = FloatToStr(red_pixels);
		  Edit6->Text = FloatToStr((double)cells_r);
		  Edit7->Text = FloatToStr(aver_r_cell_size);
		  if (aver_r_cell_size)
		   Edit8->Text = FloatToStr(red_pixels/aver_r_cell_size);
		  else
		   Edit8->Text = "";

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();

		  if (aver_r_cell_size)
		   Form1->StringGrid4->Cells[Form1->StringGrid4->Col][Form1->StringGrid4->Row] = FloatToStrF(red_pixels/aver_r_cell_size,ffFixed,4,3);
		  break;

   case 2:

 /*     Applying cell size thresholds and calculating cell size statistics for blue cells   */

		  cells_b = 0;
		  aver_b_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_b_sizes[cells_b] = cell_sizes[i];
			  aver_b_cell_size += cell_b_sizes[cells_b];
			  cells_b++;
			 }
		  }
		  if (cells_b)
		   aver_b_cell_size /= (double)cells_b;
		  else
		   aver_b_cell_size = 0.0;

		  Edit12->Text = FloatToStr(blue_pixels);
		  Edit13->Text = FloatToStr((double)cells_b);
		  Edit14->Text = FloatToStr(aver_b_cell_size);
		  if (aver_b_cell_size)
		   Edit15->Text = FloatToStr(blue_pixels/aver_b_cell_size);
		  else
		   Edit15->Text = "";

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();

		  if (aver_b_cell_size)
		   Form1->StringGrid7->Cells[Form1->StringGrid7->Col][Form1->StringGrid7->Row] = FloatToStrF(blue_pixels/aver_b_cell_size,ffFixed,4,3);
  }

  free(fname);
}

 for (i=0;i<8;++i)
  Chart1->Series[i]->Clear();

 for (i=0;i<8;++i)
  Chart1->Series[i]->Clear();

 for (j=0;j<7;++j)
 {
  green = 0.0;
  red = 0.0;
  blue = 0.0;
  ratio = 0.0;
  cnt = 0; cnt_r = 0; cnt_g = 0; cnt_b = 0;

  for (i=0;i<100;++i)
  {
   nom = 0;
   denom = 0;

   /*   Calculating sub-population	*/

   if (CheckBox2->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox4->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox6->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (CheckBox3->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox5->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox7->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (denom > 0)
	Form1->StringGrid1->Cells[j][i] = nom / denom;

	/*  Plotting graphs   */

   if (CheckListBox1->Checked[0])
   if (!(Form1->StringGrid2->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[0]->AddXY(StringGrid2->Cells[j][0].ToDouble(),Form1->StringGrid4->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[0]->AddXY(StringGrid4->Cells[j][i].ToDouble(),(double)(i+1));
	 red += Form1->StringGrid4->Cells[j][i].ToDouble();
	 cnt_r++;
	}

   if (CheckListBox1->Checked[1])
   if (!(Form1->StringGrid5->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[1]->AddXY(StringGrid5->Cells[j][0].ToDouble(),Form1->StringGrid3->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[1]->AddXY(StringGrid3->Cells[j][i].ToDouble(),(double)(i+1));
	 green += Form1->StringGrid3->Cells[j][i].ToDouble();
	 cnt_g++;
	}

   if (CheckListBox1->Checked[2])
   if (!(Form1->StringGrid6->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[2]->AddXY(StringGrid6->Cells[j][0].ToDouble(),Form1->StringGrid7->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[2]->AddXY(StringGrid7->Cells[j][i].ToDouble(),(double)(i+1));
	 blue += Form1->StringGrid7->Cells[j][i].ToDouble();
	 cnt_b++;
	}

   if (CheckListBox1->Checked[3])
   if (!(Form1->StringGrid8->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid1->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[3]->AddXY(StringGrid8->Cells[j][0].ToDouble(),Form1->StringGrid1->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart5->Series[0]->AddXY(StringGrid1->Cells[j][i].ToDouble(),(double)(i+1));
	 ratio += Form1->StringGrid1->Cells[j][i].ToDouble();
	 cnt++;
	}
  }

  if (CheckListBox1->Checked[0])
  if (cnt_r)
   Chart1->Series[4]->AddXY(StringGrid2->Cells[j][0].ToDouble(),red /(double)cnt_r);

  if (CheckListBox1->Checked[1])
  if (cnt_g)
   Chart1->Series[5]->AddXY(StringGrid5->Cells[j][0].ToDouble(),green /(double)cnt_g);

  if (CheckListBox1->Checked[2])
  if (cnt_b)
   Chart1->Series[6]->AddXY(StringGrid6->Cells[j][0].ToDouble(),blue /(double)cnt_b);

  if (CheckListBox1->Checked[3])
  if (cnt)
   Chart1->Series[7]->AddXY(StringGrid8->Cells[j][0].ToDouble(),ratio /(double)cnt);
 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar2Change(TObject *Sender)
{
 Edit10->Text = AnsiString ((long)pow(1.001,(double)TrackBar2->Position));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar3Change(TObject *Sender)
{
 Edit11->Text = AnsiString ((long)pow(1.001,(double)TrackBar3->Position));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar1Change(TObject *Sender)
{
 Edit9->Text = AnsiString (TrackBar1->Position);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject *Sender)
{
 Image2->Height = (Form1->ClientHeight - 116)/2;
 if (Image2->Width > 173)
  Label1->Left = (Image2->Width - 173)/2;

 Edit9->Left = Panel1->Width - 57;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1Click(TObject *Sender)
{
 Image4->Top = Image1->Top;
 Image4->Left = Image1->Left;
 Image4->Height = Image1->Height;
 Image4->Width = Image1->Width;
 Image4->Visible = true;
 Image4->Transparent = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image4Click(TObject *Sender)
{
 Image4->Visible = false;
 Image4->Transparent = true;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Image2MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
 int i,j;
 unsigned long ptr2;

 if (pBitmap2) delete (pBitmap2);
 Graphics::TBitmap *pBitmap2 = new Graphics::TBitmap();

 pBitmap2 = Image2->Picture->Bitmap;
 pBitmap2->PixelFormat = pf32bit;

 if (cBitmap2) free (cBitmap2);
 cBitmap2 = (unsigned long**) calloc (h, sizeof (unsigned long*));
 for (i=0;i<h;++i)
  cBitmap2[i] = (unsigned long*) pBitmap2->ScanLine[i];

 int crX = X * w / Image2->Width;
 int crY = Y * h / Image2->Height;
 unsigned long crC = cBitmap2[crY][crX];

 if (crC)
 if (crC != 0x00ff0000)
 {
  Image2->Hint = AnsiString("X="+IntToStr(crX)+";Y="+IntToStr(crY)+";C="+FloatToStr((double)crC)+";Size="+FloatToStr((double)cell_sizes[crC]));
  Image2->ShowHint = true;

  for (i=0;i<h;++i)
   for (j=0;j<w;++j)
   {
	ptr2 = cBitmap2[i][j];
	if (ptr2 == crC)
	{
	 ptr2 = 0x00ff0000;
	 cBitmap2[i][j] = ptr2;
	}
  }

  cells--;
  cell_sizes[crC] = 0;
  switch (Form1->RadioGroup2->ItemIndex)
  {
   case 0: cells_r--;
		   cell_r_sizes[crC] = 0;
		   break;
   case 1: cells_g--;
		   cell_g_sizes[crC] = 0;
		   break;
   case 2: cells_b--;
		   cell_b_sizes[crC] = 0;
  }

  Image2->Picture->Bitmap = pBitmap2;
  Image2->Refresh();
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image2DblClick(TObject *Sender)
{
 FILE *oz;
 unsigned long ptr2;
 int i,j,cnt,cnt_r,cnt_g,cnt_b,flag;
 char *fname;

if (!(CheckBox1->Checked))
{

 if (pBitmap2) delete (pBitmap2);
 Graphics::TBitmap *pBitmap2 = new Graphics::TBitmap();

 pBitmap2 = Image2->Picture->Bitmap;
 pBitmap2->PixelFormat = pf32bit;

 if (cBitmap2) free (cBitmap2);
 cBitmap2 = (unsigned long**) calloc (h, sizeof (unsigned long*));
 for (i=0;i<h;++i)
  cBitmap2[i] = (unsigned long*) pBitmap2->ScanLine[i];

 fname = (char*) calloc (1,1000);

 {
  switch (Form1->RadioGroup2->ItemIndex)
  {
   case 1:

 /*     Applying cell size thresholds and calculating cell size statistics for green cells   */

		  cells_g = 0;
		  aver_g_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_g_sizes[cells_g] = cell_sizes[i];
			  aver_g_cell_size += cell_g_sizes[cells_g];
			  cells_g++;
			 }
		  }
		  if (cells_g)
		   aver_g_cell_size /= (double)cells_g;
		  else
		   aver_g_cell_size = 0.0;

		  Edit1->Text = FloatToStr(green_pixels);
		  Edit2->Text = FloatToStr((double)cells_g);
		  Edit3->Text = FloatToStr(aver_g_cell_size);
		  if (aver_g_cell_size)
		   Edit4->Text = FloatToStr(green_pixels/aver_g_cell_size);
		  else
           Edit4->Text = "";

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if (ptr2 == 0x00ff0000)
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();

		  Form1->StringGrid3->Cells[Form1->StringGrid3->Col][Form1->StringGrid3->Row] = FloatToStrF(green_pixels/aver_g_cell_size,ffFixed,4,3);
		  break;

   case 0:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		  cells_r = 0;
		  aver_r_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_r_sizes[cells_r] = cell_sizes[i];
			  aver_r_cell_size += cell_r_sizes[cells_r];
			  cells_r++;
			 }
		  }
		  if (cells_r)
		   aver_r_cell_size /= (double)cells_r;
		  else
		   aver_r_cell_size = 0.0;

		  Edit5->Text = FloatToStr(red_pixels);
		  Edit6->Text = FloatToStr((double)cells_r);
		  Edit7->Text = FloatToStr(aver_r_cell_size);
		  if (aver_r_cell_size)
		   Edit8->Text = FloatToStr(red_pixels/aver_r_cell_size);
		  else
		   Edit8->Text = "";

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if (ptr2 == 0x00ff0000)
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();

		  Form1->StringGrid4->Cells[Form1->StringGrid4->Col][Form1->StringGrid4->Row] = FloatToStrF(red_pixels/aver_r_cell_size,ffFixed,4,3);
		  break;

   case 2:

 /*     Applying cell size thresholds and calculating cell size statistics for blue cells   */

		  cells_b = 0;
		  aver_b_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)
			 {
			  cell_b_sizes[cells_b] = cell_sizes[i];
			  aver_b_cell_size += cell_b_sizes[cells_b];
			  cells_b++;
			 }
		  }
		  if (cells_b)
		   aver_b_cell_size /= (double)cells_b;
		  else
		   aver_b_cell_size = 0.0;

		  Edit12->Text = FloatToStr(blue_pixels);
		  Edit13->Text = FloatToStr((double)cells_b);
		  Edit14->Text = FloatToStr(aver_b_cell_size);
		  if (aver_b_cell_size)
		   Edit15->Text = FloatToStr(blue_pixels/aver_b_cell_size);
		  else
		   Edit15->Text = "";

		  for (i=0;i<h;++i)
		   for (j=0;j<w;++j)
		   {
			ptr2 = cBitmap2[i][j];
			if ((cell_sizes[ptr2] < min_cell_size) || (cell_sizes[ptr2] > max_cell_size))
			{
			 ptr2 = 0;
			 cBitmap2[i][j] = ptr2;
			}
		   }

		  Image2->Picture->Bitmap = pBitmap2;
		  Image2->Refresh();

		  if (aver_b_cell_size)
		   Form1->StringGrid7->Cells[Form1->StringGrid7->Col][Form1->StringGrid7->Row] = FloatToStrF(blue_pixels/aver_b_cell_size,ffFixed,4,3);
  }

}

 for (i=0;i<8;++i)
  Chart1->Series[i]->Clear();

 for (i=0;i<3;++i)
  Chart4->Series[i]->Clear();
  Chart5->Series[0]->Clear();

 for (j=0;j<7;++j)
 {
  green = 0.0;
  red = 0.0;
  blue = 0.0;
  ratio = 0.0;
  cnt = 0; cnt_r = 0; cnt_g = 0; cnt_b = 0;

  for (i=0;i<100;++i)
  {
   nom = 0;
   denom = 0;

   /*   Calculating sub-population	*/

   if (CheckBox2->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox4->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox6->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (CheckBox3->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox5->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox7->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (denom > 0)
	Form1->StringGrid1->Cells[j][i] = nom / denom;

	/*  Plotting graphs   */

   if (CheckListBox1->Checked[0])
   if (!(Form1->StringGrid2->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[0]->AddXY(StringGrid2->Cells[j][0].ToDouble(),Form1->StringGrid4->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[0]->AddXY(StringGrid4->Cells[j][i].ToDouble(),(double)(i+1));
	 red += Form1->StringGrid4->Cells[j][i].ToDouble();
	 cnt_r++;
	}

   if (CheckListBox1->Checked[1])
   if (!(Form1->StringGrid5->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[1]->AddXY(StringGrid5->Cells[j][0].ToDouble(),Form1->StringGrid3->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[1]->AddXY(StringGrid3->Cells[j][i].ToDouble(),(double)(i+1));
	 green += Form1->StringGrid3->Cells[j][i].ToDouble();
	 cnt_g++;
	}

   if (CheckListBox1->Checked[2])
   if (!(Form1->StringGrid6->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[2]->AddXY(StringGrid6->Cells[j][0].ToDouble(),Form1->StringGrid7->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[2]->AddXY(StringGrid7->Cells[j][i].ToDouble(),(double)(i+1));
	 blue += Form1->StringGrid7->Cells[j][i].ToDouble();
	 cnt_b++;
	}

   if (CheckListBox1->Checked[3])
   if (!(Form1->StringGrid8->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid1->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[3]->AddXY(StringGrid8->Cells[j][0].ToDouble(),Form1->StringGrid1->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart5->Series[0]->AddXY(StringGrid1->Cells[j][i].ToDouble(),(double)(i+1));
	 ratio += Form1->StringGrid1->Cells[j][i].ToDouble();
	 cnt++;
	}
  }

  if (CheckListBox1->Checked[0])
  if (cnt_r)
   Chart1->Series[4]->AddXY(StringGrid2->Cells[j][0].ToDouble(),red /(double)cnt_r);

  if (CheckListBox1->Checked[1])
  if (cnt_g)
   Chart1->Series[5]->AddXY(StringGrid5->Cells[j][0].ToDouble(),green /(double)cnt_g);

  if (CheckListBox1->Checked[2])
  if (cnt_b)
   Chart1->Series[6]->AddXY(StringGrid6->Cells[j][0].ToDouble(),blue /(double)cnt_b);

  if (CheckListBox1->Checked[3])
  if (cnt)
   Chart1->Series[7]->AddXY(StringGrid8->Cells[j][0].ToDouble(),ratio /(double)cnt);
 }

 free(fname);

 }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::StringGrid2Click(TObject *Sender)
{
 for (int i=0;i<7;++i)
 {
  StringGrid5->Cells[i][0] = StringGrid2->Cells[i][0];
  StringGrid6->Cells[i][0] = StringGrid2->Cells[i][0];
  StringGrid8->Cells[i][0] = StringGrid2->Cells[i][0];
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Splitter1Moved(TObject *Sender)
{
 if (Image2->Width > 173)
  Label1->Left = (Image2->Width - 173)/2;
 Edit9->Left = Panel1->Width - 57;

 CheckListBox1->Left = Panel3->Width - 124;
 StringGrid2->Width = Panel3->Width / 4 - 2;
 StringGrid3->Width = Panel3->Width / 4 - 2;
 StringGrid4->Width = Panel3->Width / 4 - 2;
 StringGrid5->Width = Panel3->Width / 4 - 2;
 StringGrid6->Width = Panel3->Width / 4 - 2;
 StringGrid7->Width = Panel3->Width / 4 - 2;
 StringGrid8->Width = Panel3->Width / 4 - 2;

}
//---------------------------------------------------------------------------



void __fastcall TForm1::StringGrid7Click(TObject *Sender)
{
 RadioGroup2->ItemIndex = 2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox2Click(TObject *Sender)
{
 static int flagr;
 int i,j,cnt,cnt_r,cnt_g,cnt_b;

 for (i=0;i<8;++i)
  Chart1->Series[i]->Clear();

 for (i=0;i<3;++i)
  Chart4->Series[i]->Clear();
  Chart5->Series[0]->Clear();

 for (j=0;j<7;++j)
 {
  green = 0.0;
  red = 0.0;
  blue = 0.0;
  ratio = 0.0;
  cnt = 0; cnt_r = 0; cnt_g = 0; cnt_b = 0;

  for (i=0;i<100;++i)
  {
   nom = 0;
   denom = 0;

   /*   Calculating sub-population	*/

   if (CheckBox2->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox4->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox6->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 nom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (CheckBox3->Checked)
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid4->Cells[j][i].ToDouble();

   if (CheckBox5->Checked)
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid3->Cells[j][i].ToDouble();

   if (CheckBox7->Checked)
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	 denom += Form1->StringGrid7->Cells[j][i].ToDouble();

   if (denom > 0)
	Form1->StringGrid1->Cells[j][i] = nom / denom;

	/*  Plotting graphs   */

   if (CheckListBox1->Checked[0])
   if (!(Form1->StringGrid2->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid4->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[0]->AddXY(StringGrid2->Cells[j][0].ToDouble(),Form1->StringGrid4->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[0]->AddXY(StringGrid4->Cells[j][i].ToDouble(),(double)(i+1));
	 red += Form1->StringGrid4->Cells[j][i].ToDouble();
	 cnt_r++;
	}

   if (CheckListBox1->Checked[1])
   if (!(Form1->StringGrid5->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid3->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[1]->AddXY(StringGrid5->Cells[j][0].ToDouble(),Form1->StringGrid3->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[1]->AddXY(StringGrid3->Cells[j][i].ToDouble(),(double)(i+1));
	 green += Form1->StringGrid3->Cells[j][i].ToDouble();
	 cnt_g++;
	}

   if (CheckListBox1->Checked[2])
   if (!(Form1->StringGrid6->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid7->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[2]->AddXY(StringGrid6->Cells[j][0].ToDouble(),Form1->StringGrid7->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart4->Series[2]->AddXY(StringGrid7->Cells[j][i].ToDouble(),(double)(i+1));
	 blue += Form1->StringGrid7->Cells[j][i].ToDouble();
	 cnt_b++;
	}

   if (CheckListBox1->Checked[3])
   {
	if (UpDown1->Top < 0)
	{
	 TabControl1->TabIndex = 0;
	 Chart1->Visible = false;
	 Chart2->Visible = false;
	 Chart3->Visible = false;
	 Chart4->Visible = true;
	 Chart5->Visible = true;
	 TabControl1->Height = (TabControl1->Height - 32) * 2 + 32;
	 UpDown1->Top = 1;
	}

	if (!(Form1->StringGrid8->Cells[j][0].IsEmpty()))
	if (!(Form1->StringGrid1->Cells[j][i].IsEmpty()))
	{
	 Chart1->Series[3]->AddXY(StringGrid8->Cells[j][0].ToDouble(),Form1->StringGrid1->Cells[j][i].ToDouble());
	 if (j == StringGrid2->Col)
	  Chart5->Series[0]->AddXY(StringGrid1->Cells[j][i].ToDouble(),(double)(i+1));
	 ratio += Form1->StringGrid1->Cells[j][i].ToDouble();
	 cnt++;
	}
   }
   else
   {
	if (UpDown1->Top > 0)
	{
	 TabControl1->TabIndex = 0;
	 Chart1->Visible = false;
	 Chart2->Visible = false;
	 Chart3->Visible = false;
	 Chart4->Visible = true;
	 Chart5->Visible = false;
	 TabControl1->Height = (TabControl1->Height - 32) / 2 + 32;
	 UpDown1->Top = -23;
	}
   }
  }

  if (CheckListBox1->Checked[0])
  if (cnt_r)
   Chart1->Series[4]->AddXY(StringGrid2->Cells[j][0].ToDouble(),red /(double)cnt_r);

  if (CheckListBox1->Checked[1])
  if (cnt_g)
   Chart1->Series[5]->AddXY(StringGrid5->Cells[j][0].ToDouble(),green /(double)cnt_g);

  if (CheckListBox1->Checked[2])
  if (cnt_b)
   Chart1->Series[6]->AddXY(StringGrid6->Cells[j][0].ToDouble(),blue /(double)cnt_b);

  if (CheckListBox1->Checked[3])
  if (cnt)
   Chart1->Series[7]->AddXY(StringGrid8->Cells[j][0].ToDouble(),ratio /(double)cnt);
 }

}
//---------------------------------------------------------------------------


void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{
 if (CheckBox1->Checked)
 {
  Button3->Enabled = false;

  Edit2->Enabled = false;
  Edit6->Enabled = false;
  Edit13->Enabled = false;

  Edit3->Enabled = false;
  Edit7->Enabled = false;
  Edit14->Enabled = false;
 }
 else
 {
  Button3->Enabled = true;

  Edit2->Enabled = true;
  Edit6->Enabled = true;
  Edit13->Enabled = true;

  Edit3->Enabled = true;
  Edit7->Enabled = true;
  Edit14->Enabled = true;
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
 TRGBTriple ptr;
 unsigned long ptr2, ptr3, ptr4, ptr5, ptr6, getmin, ptrx, S, CS;
 double ptr1;
 int i,j,k,cnt,cnt_r,cnt_g,cnt_b,flag,ii,jj;

 if (pBitmap) delete (pBitmap);
 if (pBitmap2) delete (pBitmap2);
 if (pBitmap3) delete (pBitmap3);
 Graphics::TBitmap *pBitmap = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap2 = new Graphics::TBitmap();
 Graphics::TBitmap *pBitmap3 = new Graphics::TBitmap();

 for (i=0;i<11;++i)
 {
  Chart2->Series[i]->Clear();
  Chart3->Series[i]->Clear();
 }

 CGauge1->MaxValue = 250 * (ComboBox1->ItemIndex * 2 + 1);

 for (TrackBar1->Position = 1; TrackBar1->Position <= 250; TrackBar1->Position++)
 {

  green_pixels = 0.0;
  red_pixels = 0.0;
  blue_pixels = 0.0;
  cells = 0;
  cells_g = 0;
  cells_r = 0;
  cells_b = 0;

  Image4->Picture->LoadFromFile(AnsiString(path));
  Image4->Refresh();

 pBitmap->Assign(Form1->Image4->Picture->Graphic);
 pBitmap2->Assign(Form1->Image4->Picture->Graphic);
 pBitmap3->Assign(Form1->Image4->Picture->Graphic);

 h = pBitmap->Height;
 w = pBitmap->Width;

 pBitmap->PixelFormat = pf24bit;
 pBitmap2->PixelFormat = pf32bit;
 pBitmap3->PixelFormat = pf32bit;

 pBitmap2->Canvas->Brush->Color = (TColor)0;
 pBitmap2->Canvas->FillRect(Rect(0,0,w,h));

 if (cBitmap1) free (cBitmap1);
 if (cBitmap2) free (cBitmap2);
 cBitmap1 = (TRGBTriple**) calloc (h, sizeof (TRGBTriple*));
 cBitmap2 = (unsigned long**) calloc (h, sizeof (unsigned long*));
 for (i=0;i<h;++i)
 {
  cBitmap1[i] = (TRGBTriple*) pBitmap->ScanLine[i];
  cBitmap2[i] = (unsigned long*) pBitmap2->ScanLine[i];
 }

 if (cell_sizes) free(cell_sizes);
 if (cell_g_sizes) free(cell_g_sizes);
 if (cell_r_sizes) free(cell_r_sizes);
 if (cell_b_sizes) free(cell_b_sizes);
 cell_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_g_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_r_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));
 cell_b_sizes = (unsigned long*) calloc (h*w*6, sizeof (unsigned long));

  segm_scan();

  Image1->Picture->Bitmap = pBitmap;
  Image1->Refresh();

  if (!(TrackBar2->Position))
  {
   TrackBar2->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar2->Frequency = 1;
  }

  if (TrackBar3->Position == TrackBar3->Max)
  {
   TrackBar3->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar3->Frequency = 1;
   TrackBar3->Position = (int)(log((double)(h*w))/log(1.001));
  }

  if (!(TrackBar4->Position))
  {
   TrackBar4->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar4->Frequency = 1;
  }

  if (!(TrackBar5->Position))
  {
   TrackBar5->Max = (int)(log((double)(h*w))/log(1.001));
   TrackBar5->Frequency = 1;
  }

  min_cell_size = (long)pow(1.001,(double)TrackBar2->Position);
  max_cell_size = (long)pow(1.001,(double)TrackBar3->Position);

  pBitmap3->Canvas->CopyRect(Rect(0,0,w,h),pBitmap2->Canvas,Rect(0,0,w,h));
  Image3->Picture->Bitmap = pBitmap3;

  switch (Form1->RadioGroup2->ItemIndex)
  {
   case 1:

 /*     Applying cell size thresholds and calculating cell size statistics for green cells   */

		 {
		  cells_g = 0;
		  aver_g_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
/*			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)*/
			 {
			  cell_g_sizes[cells_g] = cell_sizes[i];
			  aver_g_cell_size += cell_g_sizes[cells_g];
			  cells_g++;
			 }
		  }
		  if (cells_g)
		   aver_g_cell_size /= (double)cells_g;
		  else
		   aver_g_cell_size = 0.0;

		  if (green_pixels > 0)
		   Chart2->Series[0]->AddXY(TrackBar1->Position, green_pixels);
		  if (cells_g > 0)
		   Chart3->Series[0]->AddXY(TrackBar1->Position, cells_g);

		  std::sort(cell_g_sizes, cell_g_sizes + cells_g);

		  i = 0;
          k = 0;
		  for (S = Edit16->Text.ToInt(); S <= Edit17->Text.ToInt(); S += Edit16->Text.ToInt())
		  {
		   ++k;
		   j = 0;
		   CS = 0;
		   while (cell_g_sizes[i] < S)
		   {
			if (i >= cells_g)
			 break;
			CS += cell_g_sizes[i];
			++i;
			++j;
		   }
		   if (!(CheckBox8->Checked))
		   {
			if (k < 11)
			{
			 if (CS > 0)
			  Chart2->Series[k]->AddXY(TrackBar1->Position, CS);
			 if (j > 0)
			  Chart3->Series[k]->AddXY(TrackBar1->Position, j);
			}

		   }
		   else
		   {
			if (k < 11)
			{
			 if (CS > 0)
			  Chart2->Series[k]->AddXY(TrackBar1->Position, (double)CS/green_pixels);
			 if (j > 0)
			  Chart3->Series[k]->AddXY(TrackBar1->Position, (double)j/(double)cells_g);
			}

		   }
		  }
		 }
		 break;

   case 0:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		 {
		  cells_r = 0;
		  aver_r_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
/*			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)*/
			 {
			  cell_r_sizes[cells_r] = cell_sizes[i];
			  aver_r_cell_size += cell_r_sizes[cells_r];
			  cells_r++;
			 }
		  }
		  if (cells_r)
		   aver_r_cell_size /= (double)cells_r;
		  else
		   aver_r_cell_size = 0.0;

		  if (red_pixels > 0)
		   Chart2->Series[0]->AddXY(TrackBar1->Position, red_pixels);
		  if (cells_r > 0)
		   Chart3->Series[0]->AddXY(TrackBar1->Position, cells_r);

		  std::sort(cell_r_sizes, cell_r_sizes + cells_r);

		  i = 0;
		  k = 0;
		  for (S = Edit16->Text.ToInt(); S <= Edit17->Text.ToInt(); S += Edit16->Text.ToInt())
		  {
		   ++k;
		   j = 0;
		   CS = 0;
		   while (cell_r_sizes[i] < S)
		   {
			if (i >= cells_r)
			 break;
			CS += cell_r_sizes[i];
			++i;
            ++j;
		   }
		   if (!(CheckBox8->Checked))
		   {
			if (k < 11)
			{
			 if (CS > 0)
			  Chart2->Series[k]->AddXY(TrackBar1->Position, CS);
			 if (j > 0)
			  Chart3->Series[k]->AddXY(TrackBar1->Position, j);
			}
		   }
		   else
		   {
			if (k < 11)
			{
			 if (CS > 0)
			  Chart2->Series[k]->AddXY(TrackBar1->Position, (double)CS/red_pixels);
			 if (j > 0)
			  Chart3->Series[k]->AddXY(TrackBar1->Position, (double)j/(double)cells_r);
            }
		   }
		  }
		 }
		 break;

   case 2:

 /*     Applying cell size thresholds and calculating cell size statistics for red cells   */

		 {
		  cells_b = 0;
		  aver_b_cell_size = 0.0;
		  for (i=0; i<h*w*6; ++i)
		  {
		   if (cell_sizes[i])
/*			if (cell_sizes[i] > min_cell_size)
			 if (cell_sizes[i] < max_cell_size)*/
			 {
			  cell_b_sizes[cells_b] = cell_sizes[i];
			  aver_b_cell_size += cell_b_sizes[cells_b];
			  cells_b++;
			 }
		  }
		  if (cells_b)
		   aver_b_cell_size /= (double)cells_b;
		  else
		   aver_b_cell_size = 0.0;

		  if (blue_pixels > 0)
		   Chart2->Series[0]->AddXY(TrackBar1->Position, blue_pixels);
		  if (cells_b > 0)
		   Chart3->Series[0]->AddXY(TrackBar1->Position, cells_b);

		  std::sort(cell_b_sizes, cell_b_sizes + cells_b);

		  i = 0;
		  k = 0;
		  for (S = Edit16->Text.ToInt(); S <= Edit17->Text.ToInt(); S += Edit16->Text.ToInt())
		  {
		   ++k;
		   j = 0;
		   CS = 0;
		   while (cell_b_sizes[i] < S)
		   {
			if (i >= cells_b)
			 break;
			CS += cell_b_sizes[i];
			++i;
			++j;
		   }
		   if (!(CheckBox8->Checked))
		   {
			if (k < 11)
			{
			 if (CS > 0)
			  Chart2->Series[k]->AddXY(TrackBar1->Position, CS);
			 if (j > 0)
			  Chart3->Series[k]->AddXY(TrackBar1->Position, j);
			}
		   }
		   else
		   {
			if (k < 11)
			{
			 if (CS > 0)
			  Chart2->Series[k]->AddXY(TrackBar1->Position, (double)CS/blue_pixels);
			 if (j > 0)
			  Chart3->Series[k]->AddXY(TrackBar1->Position, (double)j/(double)cells_b);
            }
		   }
		  }
		 }

  }
 }

 Chart2->Series[0]->Title = "Total";
 Chart3->Series[0]->Title = "Total";
 k = 0;
 for (S = Edit16->Text.ToInt(); S <= Edit17->Text.ToInt(); S += Edit16->Text.ToInt())
 {
  ++k;
  Chart2->Series[k]->Title = AnsiString(S-Edit16->Text.ToInt()+1)+"-"+AnsiString(S);
  Chart3->Series[k]->Title = AnsiString(S-Edit16->Text.ToInt()+1)+"-"+AnsiString(S);
 }

 if (UpDown1->Top < 0)
 {
  TabControl1->TabIndex = 1;
  Chart2->Visible = true;
  Chart3->Visible = true;
  TabControl1->Height = (TabControl1->Height - 32) * 2 + 32;
  UpDown1->Top = 1;
  UpDown1->Position = -1;
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar4Change(TObject *Sender)
{
 Edit16->Text = AnsiString ((long)pow(1.001,(double)TrackBar4->Position));
 if (TrackBar5->Position < TrackBar4->Position)
  TrackBar5->Position = TrackBar4->Position;
 if (pow(1.001,(double)TrackBar5->Position) > pow(1.001,(double)TrackBar4->Position) * 10)
  TrackBar5->Position = log(pow(1.001,(double)TrackBar4->Position) * 10)/log(1.001);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar5Change(TObject *Sender)
{
 Edit17->Text = AnsiString ((long)pow(1.001,(double)TrackBar5->Position));
 if (TrackBar5->Position < TrackBar4->Position)
  TrackBar5->Position = TrackBar4->Position;
 if (pow(1.001,(double)TrackBar5->Position) > pow(1.001,(double)TrackBar4->Position) * 10)
  TrackBar5->Position = log(pow(1.001,(double)TrackBar4->Position) * 10)/log(1.001);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TabControl1Change(TObject *Sender)
{
 if (UpDown1->Top > 0)
 {
  TabControl1->Height = (TabControl1->Height - 32) / 2 + 32;
  UpDown1->Top = -23;
  Chart3->Visible = false;
  Chart5->Visible = false;
 }
 switch(TabControl1->TabIndex)
 {
  case 0: Chart1->Visible = false;
		  Chart2->Visible = false;
		  Chart3->Visible = false;
		  Chart4->Visible = true;
		  Chart5->Visible = false;
		  break;
  case 1: Chart1->Visible = true;
		  Chart2->Visible = false;
		  Chart3->Visible = false;
		  Chart4->Visible = false;
		  Chart5->Visible = false;
		  break;
  case 2: Chart1->Visible = false;
		  Chart2->Visible = true;
		  Chart3->Visible = false;
		  Chart4->Visible = false;
		  Chart5->Visible = false;
		  break;
  case 3: Chart1->Visible = false;
		  Chart2->Visible = false;
		  Chart3->Visible = true;
		  Chart4->Visible = false;
		  Chart5->Visible = false;
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::UpDown1Click(TObject *Sender, TUDBtnType Button)
{
 if (UpDown1->Top < 0)
 {
  Chart1->Visible = false;
  if (TabControl1->TabIndex < 2)
  {
   TabControl1->TabIndex = 0;
   Chart2->Visible = false;
   Chart3->Visible = false;
   Chart4->Visible = true;
   Chart5->Visible = true;
  }
  else
  {
   TabControl1->TabIndex = 3;
   Chart2->Visible = true;
   Chart3->Visible = true;
   Chart4->Visible = false;
   Chart5->Visible = false;
  }
  TabControl1->Height = (TabControl1->Height - 32) * 2 + 32;
  UpDown1->Top = 1;
 }
 else
 {
  TabControl1->Height = (TabControl1->Height - 32) / 2 + 32;
  UpDown1->Top = -23;
  Chart3->Visible = false;
  Chart5->Visible = false;
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Chart2ClickSeries(TCustomChart *Sender, TChartSeries *Series,
          int ValueIndex, TMouseButton Button, TShiftState Shift, int X,
          int Y)
{
 int k,S;

 TrackBar1->Position = Series->XValue[ValueIndex];

 k = 0;
 for (S = Edit16->Text.ToInt(); S <= Edit17->Text.ToInt(); S += Edit16->Text.ToInt())
 {
  ++k;
  if (Chart2->Series[k]->Clicked(X,Y) > 0)
  if (Chart2->Series[k]->Active)
  {
   Edit10->Text = AnsiString(S - Edit16->Text.ToInt() + 1);
   TrackBar2->Position = log(Edit10->Text.ToDouble())/log(1.001);

   Edit11->Text = AnsiString(S);
   TrackBar3->Position = log(Edit11->Text.ToDouble())/log(1.001);
  }

  if (Chart2->Series[k]->Clicked(X,Y) > 0)
   break;
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Chart3ClickSeries(TCustomChart *Sender, TChartSeries *Series,
		  int ValueIndex, TMouseButton Button, TShiftState Shift, int X,
		  int Y)
{
 int k,S;

 TrackBar1->Position = Series->XValue[ValueIndex];

 k = 0;
 for (S = Edit16->Text.ToInt(); S <= Edit17->Text.ToInt(); S += Edit16->Text.ToInt())
 {
  ++k;
  if (Chart3->Series[k]->Clicked(X,Y) > 0)
  if (Chart3->Series[k]->Active)
  {
   Edit10->Text = AnsiString(S - Edit16->Text.ToInt() + 1);
   TrackBar2->Position = log(Edit10->Text.ToDouble())/log(1.001);

   Edit11->Text = AnsiString(S);
   TrackBar3->Position = log(Edit11->Text.ToDouble())/log(1.001);
  }

  if (Chart3->Series[k]->Clicked(X,Y) > 0)
   break;
 }

}
//---------------------------------------------------------------------------

