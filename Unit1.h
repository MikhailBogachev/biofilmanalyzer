//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Buttons.hpp>
#include "cgauges.h"
#include <VCLTee.TeeFunci.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <Vcl.CheckLst.hpp>
#undef Pi
#include <OleServer.hpp>
#include <Menus.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TPanel *Panel2;
        TRadioGroup *RadioGroup2;
        TRadioGroup *RadioGroup3;
        TCGauge *CGauge1;
        TImage *Image1;
        TPanel *Panel3;
        TImage *Image2;
        TImage *Image3;
        TPanel *Panel4;
        TTrackBar *TrackBar1;
        TEdit *Edit9;
        TLabel *Label2;
        TLabel *Label1;
	TImage *Image4;
	TPanel *Panel7;
	TStringGrid *StringGrid3;
	TStringGrid *StringGrid4;
	TStringGrid *StringGrid1;
	TPanel *Panel8;
	TStringGrid *StringGrid2;
	TStringGrid *StringGrid5;
	TStringGrid *StringGrid6;
	TSplitter *Splitter1;
	TStringGrid *StringGrid7;
	TStringGrid *StringGrid8;
	TGroupBox *GroupBox3;
	TCheckBox *CheckBox2;
	TCheckBox *CheckBox3;
	TCheckBox *CheckBox4;
	TCheckBox *CheckBox5;
	TCheckBox *CheckBox6;
	TCheckBox *CheckBox7;
	TLabel *Label17;
	TScrollBox *ScrollBox1;
	TPanel *Panel6;
	TLabel *Label10;
	TLabel *Label11;
	TLabel *Label12;
	TLabel *Label13;
	TPanel *Panel5;
	TGroupBox *GroupBox1;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TEdit *Edit1;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit4;
	TGroupBox *GroupBox2;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label3;
	TEdit *Edit5;
	TEdit *Edit6;
	TEdit *Edit7;
	TEdit *Edit8;
	TGroupBox *GroupBox4;
	TLabel *Label18;
	TLabel *Label19;
	TLabel *Label20;
	TEdit *Edit12;
	TEdit *Edit13;
	TEdit *Edit14;
	TEdit *Edit15;
	TTrackBar *TrackBar3;
	TTrackBar *TrackBar2;
	TComboBox *ComboBox1;
	TButton *Button3;
	TButton *Button1;
	TCheckBox *CheckBox1;
	TEdit *Edit10;
	TEdit *Edit11;
	TButton *Button2;
	TGroupBox *GroupBox5;
	TButton *Button4;
	TCheckBox *CheckBox8;
	TLabel *Label21;
	TLabel *Label22;
	TEdit *Edit16;
	TEdit *Edit17;
	TTrackBar *TrackBar4;
	TTrackBar *TrackBar5;
	TTabControl *TabControl1;
	TChart *Chart1;
	TLabel *Label9;
	TLabel *Label14;
	TLabel *Label15;
	TLabel *Label16;
	TPointSeries *Series2;
	TPointSeries *Series1;
	TPointSeries *Series7;
	TPointSeries *Series5;
	TLineSeries *Series4;
	TAverageTeeFunction *TeeFunction2;
	TLineSeries *Series3;
	TAverageTeeFunction *TeeFunction1;
	TLineSeries *Series6;
	TAverageTeeFunction *TeeFunction3;
	TLineSeries *Series8;
	TAverageTeeFunction *TeeFunction4;
	TChart *Chart2;
	TPointSeries *Series9;
	TPointSeries *Series10;
	TPointSeries *Series11;
	TPointSeries *Series12;
	TPointSeries *Series13;
	TPointSeries *Series14;
	TPointSeries *Series15;
	TPointSeries *Series16;
	TPointSeries *Series17;
	TPointSeries *Series18;
	TPointSeries *Series19;
	TChart *Chart3;
	TPointSeries *PointSeries1;
	TPointSeries *PointSeries2;
	TPointSeries *PointSeries3;
	TPointSeries *PointSeries4;
	TPointSeries *PointSeries5;
	TPointSeries *PointSeries6;
	TPointSeries *PointSeries7;
	TPointSeries *PointSeries8;
	TPointSeries *PointSeries9;
	TPointSeries *PointSeries10;
	TPointSeries *PointSeries11;
	TUpDown *UpDown1;
	TChart *Chart4;
	TLabel *Label23;
	TLabel *Label24;
	TLabel *Label25;
	TLabel *Label26;
	THorizBarSeries *Series20;
	THorizBarSeries *Series21;
	THorizBarSeries *Series22;
	TChart *Chart5;
	TLabel *Label27;
	TLabel *Label28;
	TLabel *Label29;
	TLabel *Label30;
	THorizBarSeries *HorizBarSeries1;
	TCheckListBox *CheckListBox1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall StringGrid3Click(TObject *Sender);
        void __fastcall StringGrid4Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall TrackBar2Change(TObject *Sender);
        void __fastcall TrackBar3Change(TObject *Sender);
        void __fastcall TrackBar1Change(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall Image4Click(TObject *Sender);
	void __fastcall Image2MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Image2DblClick(TObject *Sender);
	void __fastcall StringGrid2Click(TObject *Sender);
	void __fastcall Splitter1Moved(TObject *Sender);
	void __fastcall StringGrid7Click(TObject *Sender);
	void __fastcall CheckBox2Click(TObject *Sender);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall TrackBar4Change(TObject *Sender);
	void __fastcall TrackBar5Change(TObject *Sender);
	void __fastcall TabControl1Change(TObject *Sender);
	void __fastcall UpDown1Click(TObject *Sender, TUDBtnType Button);
	void __fastcall Chart2ClickSeries(TCustomChart *Sender, TChartSeries *Series, int ValueIndex,
          TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall Chart3ClickSeries(TCustomChart *Sender, TChartSeries *Series, int ValueIndex,
          TMouseButton Button, TShiftState Shift, int X, int Y);
private:	// User declarations
public:		// User declarations
void virtual __fastcall WMDropFiles(TWMDropFiles &Message);
        __fastcall TForm1(TComponent* Owner);
BEGIN_MESSAGE_MAP
MESSAGE_HANDLER(WM_DROPFILES,TWMDropFiles,WMDropFiles)
END_MESSAGE_MAP(TForm);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
